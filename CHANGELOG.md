# Changelog

Until there is a dedicated guide how to use this log, please stick to this article:
https://keepachangelog.com/de/0.3.0/

Please pick from the following sections when categorizing your entry:
`Added`, `Changed`, `Removed`, `Fixed`

and put the following change severity in brackets (see the above link for details):
`Major`, `Minor`, `Patch`

## Unreleased

### Changed
- [Patch] Updated installation instructions.

## [1.6.2] - 2021-08-03

### Removed
- [Patch] Removed all references to kiteswarms.com

## [1.6.1] - 2021-07-23
### Changed
- [Patch] Changed gitlab job definitions to install dependencies via direct apt call
          and changed path to docker registry.

## [1.6.0] - 2021-07-22

### Added
- [Patch] GPLv3 license.
- [Patch] Added generated messages to .gitignore.

## [1.5.1] - 2021-07-08

- [Minor] Use relative paths in the message generation cmake to make it more portable.

## [1.5.0] - 2021-07-06

### Changed
- [Minor] Changed the way in which custom message serializers are implemented.

### Removed
- [Patch] Dependency to zcom.
- [Patch] Implicit dependency to kiteswarms-cicd scripts.
- [Patch] Dependency to kiteswarms-utils and kiteswarms-time-utils.
  
### Added
- [Patch] pulicast_messages for pulicast internal use such as distributed settings and discovery.

## [1.4.5] - 2021-06-10
### Fixed
* [Patch] Fix the implementation of operator-> in the DistributedSetting.

## [1.4.4] - 2021-06-01
### Fixed
* [Patch] Replace stream operators in serialization code.

## [1.4.3] - 2021-05-28
### Fixed
* [Patch] Fixed memory issue due to wrong usage of asio timer.
* [Patch] Improved the examples documentation.
* [Patch] Improved the documentation in the top-level namespace.

## [1.4.2] - 2021-05-27
### Fixed
* [Patch] Fix memory corruption bug due to weird ownership relations.

## [1.4.1] - 2021-05-21
### Added
* [Patch] Make debian packages availble over package registry via ci job

### Changed
* [Minor] Distributed settings now use pike, reset and unset messages.
* [Minor] Distributed settings now support all serializable types as value type

## [1.4.0] - 2021-05-06

### Added
* [Minor] Added a tool to replay zcm logfiles recorded using pulilog.

### Changed
* [Minor] Distributed settings now use the new roc_ack messages for communication and accept namespaces.

## [1.3.0] - 2021-04-09

### Fixed
* [Patch] Added the kiteswarms-utils library as a dependency since it is needed for puliping.

### Changed
* [Minor] Decoupled the serialization system from the channel implementation.
* [Minor] Pulicast logger: Whitespaces in names of logfiles replaced by '_'

## [1.2.0] - 2021-04-09

### Added
* [Minor] Channels operate in namespaces.
* [Patch] Add merge request template.

### Fixed
* [Patch] ASIO transports throws errors in subscription callbacks after node gets destroyed

## [1.1.5] - 2021-04-01

### Fixed
* [Patch] Fixed bug that caused an exception when receiving data with too many threads.
* [Patch] Make pipeline point to master of kiteswarms/kiteswarms-gitlab-ci again.

## [1.1.4] - 2021-03-08

### Added
* [Minor] Added network interface address parameter to the node constructor.
* [Minor] Command line arguments to specify the interface address parameters to the apps.

### Changed
* [Minor] Puliping is an install target
* [Minor] Packets are not sent with the IP_MULTICAST_LOOP enabled.

### Fixed
* [Patch] Added ttl option to puliping

## [1.1.3] - 2021-02-15

### Fixed
* [Patch] Fixed bug that caused us to bleed multicast messages even if ttl=0

## [1.1.2] - 2021-02-11

### Added
* [Patch] Experimental Distributed Settings

### Changed
* [Patch] Switch back to master ref for clang-tidy ci job

* [Patch] Improved test suite.

## [1.1.1] - 2021-01-11

### Fixed
* [Patch] Fixed bug which kept the AsioChannelReceiver alive for too long.

## [1.1.0] - 2021-01-07

### Added
* [Minor] Add `wrap` functions to wrap a subscription handler in an `asio::strand`.

### Changed
* [Patch] Various minor code beauty improvements and documentation updates.
* [Patch] Changed the pulicast target to `pulicast::pulicast_asio` and `pulicast::pulicast_asio_multithreaded`
* [Patch] Switch from `asio::strand` to `asio::io_service::strand` to switch to new asio version.
* [Patch] Switch .gitlab-ci standard jobs back to ref  master as it now uses per default ubuntu20.

### Removed
* [Patch] Removed some `boost::posix_time` dependencies.
* [Patch] Disabled some rotten tests (temporarily).

## [1.0.0] - 2020-12-04

### Fixed
* [Patch] Fixed Memcheck Pipeline and Property Test
* [Patch] Removed obsolete <iostream> include from channel.h


### Changed
* [Major] Simplify the interface to the transport layer and thereby change the wire format. 
  CAN NOT COMMUNICATE WITH OLDER PULICAST VERSIONS!
* [Patch] Simplify the channel implementation.

## [0.12.0] - 2020-11-19

### Added
* [Patch] Add custom clang-tidy cpp linter job that scans apps examples and include for cpp and h(pp) files
* [Minor] Add flag to append date and time to the log file name to pulilog.

## [0.11.1] - 2020-11-16

### Changed
* [Patch] Fix memory leak upon destruction of AsioNode.

## [0.11.0] - 2020-11-11

### Removed

* [Minor] Make the ZCM bridge not forward all channels from ZCM to pulicast by default.

## [0.10.0] - 2020-11-10

### Removed
* [Patch] Removed convenience import of asio/asio.hpp

### Changed
* [Patch] Minor fixes in examples.
* [Minor] Use std::chrono::duration for publishing periods and announcement intervals.

### Added
 * [Patch] DEFAULT_PORT constant.

## [0.9.0] - 2020-11-05

### Changed
* [Major] Simplify the AsioNode constructor.

## [0.8.0] - 2020-11-05

### Changed
* [Minor] Fixed the CI/CD pipeline to actually fail when the tests fail
* [Minor] Fixed includes in property tests
* [Minor] The puli_zcm_bridge now forwards all used channels, not just published ones to ZCM.

## [0.7.0-beta] - 2020-10-29

### Added
* [Patch] Add Covertura coverage target for cicd and add kiteswarms-zcom to debian dependencies
* [Patch] Added more Unit Tests

### Changed
* [Minor] Fix bug in session ID assignment.
* [Minor] Fix bug in node tests.
* [Patch] Use std::chrono where applicable.
* [Minor] Reorganized and renamed some classes so that users don't have to access `pulicast::core`.

### Removed
* [Patch] The documentation for development tools was removed since it is moved to the python docu.

## [0.6.0-beta] - 2020-09-29

### Changed
* [Minor] The puli_zcm_bridge now does not automatically forward the __nodes channel any more.

### Added
* [Patch] Add Covertura coverage target for cicd and add kiteswarms-zcom to debian dependencies
* [Minor] Modern Cmake targets with coverage and memcheck. Added dependencies via find_package.
* [Patch] Doxygen setup with CI job.
* [Major] Remove template parameters from Channel and Node objects to simplify API. This breaks the current API!
* [Major] Change the API for custom transport implementations and unify it in one abstract class.
* [Minor] Update the README.md to the current API state and make it point to the python documentation.
* [Minor] Add examples to the documentation.
* [Minor] Add API documentation for most important classes.
* [Minor] Add puliping tool
* [Minor] Add zcmping tool
* [Minor] Add startup delay to ping tools.


## [0.5.0-beta] - 2020-08-19

### Changed
* [Minor] use `asio::buffer` instead of own buffer implementation (feasibility with embedded has been checked).
* [Minor] Add some unit tests.
* [Minor] Change the signature of the MulticastSender::Send method to accept generic containers of asio buffers.


### Added
* [Patch] Added more unit tests

### Removed
* [Minor] Remove global Channel typedef and move it into the Node class.


## [0.4.1-beta] - 2020-07-06

### Changed
* [Patch] Fix ci-cd by enabling old build job again.

## [0.4.0-beta] - 2020-07-06

### Changed
* [Patch] Simplify the gitlab CI script by relying on the kiteswarms-gitlab-ci repository.

### Added
* [Patch] Add a changelog
