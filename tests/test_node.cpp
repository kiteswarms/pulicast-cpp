/*     Copyright (C) 2021 Kiteswarms Ltd
 *
 *     This file is part of pulicast.
 *
 *     pulicast is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     pulicast is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with pulicast.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <string>
#include <chrono>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <asio.hpp>
#include <pulicast/node.h>
#include <pulicast/pulicast.h>

#include "utils/mock_node.h"

using namespace std::chrono_literals;

TEST(NodeTest, GetterTest) { //NOLINT
  std::string node_name = "name";
  pulicast::PublishingPeriod announcement_period_ns = 100ms;

  auto node = pulicast::testutils::MockNode(node_name, announcement_period_ns);

  EXPECT_EQ(node.GetName(), node_name);
  EXPECT_EQ(node.GetAnnouncementPeriod(), announcement_period_ns);
}

TEST(NodeTest, SessionIDTest) { //NOLINT

  for(int i = 0; i < 1000; i++) {
    auto n1 = pulicast::testutils::MockNode("n1", 100ms);
    auto n2 = pulicast::testutils::MockNode("n2", 100ms);
    EXPECT_NE(n1.GetSessionID(), n2.GetSessionID());
    EXPECT_GE(n1.GetSessionID(), 0);
    EXPECT_GE(n2.GetSessionID(), 0);
    EXPECT_LE(n1.GetSessionID(), std::numeric_limits<int64_t>::max());
    EXPECT_LE(n2.GetSessionID(), std::numeric_limits<int64_t>::max());
  }
}

TEST(NodeTest, test_that_something_is_published_on_nodes_channel_when_nodes_is_announced) { //NOLINT
  // GIVEN
  auto node = pulicast::testutils::MockNode("name");

  // THEN
  EXPECT_CALL(node.GetChannelSender("__nodes"), Send).Times(1);

  // WHEN
  node.AnnounceNode();
}


TEST(NodeTest, test_that_something_is_published_on_nodes_channel_when_new_channel_is_used) { //NOLINT
  // GIVEN
  auto node = pulicast::testutils::MockNode("name");

  // THEN
  EXPECT_CALL(node.GetChannelSender("__nodes"), Send).Times(1);

  // WHEN
  auto& channel = node["channel"];  // just using the channel here should trigger an announce.
}

TEST(NodeTest, test_that_node_can_be_used_as_namespace) {  //NOLINT
  //GIVEN
  auto node = pulicast::testutils::MockNode("name", "my_namespace");

  auto use_as_ns = [](pulicast::Namespace& ns) {
    // THEN
    EXPECT_EQ(ns["my_channel"].GetName(), "my_namespace/my_channel");
  };

  // WHEN
  use_as_ns(node);
}

TEST(NodeTest, test_that_new_namespace_is_not_in_node_default_namespace) {  //NOLINT
  //GIVEN
  auto node = pulicast::testutils::MockNode("name", "my_namespace");

  auto use_as_ns = [](pulicast::Namespace& ns) {
    // THEN
    EXPECT_EQ(ns["my_channel"].GetName(), "other_namespace/my_channel");
  };

  // WHEN
  auto ns = node.MakeNamespace("other_namespace");
  use_as_ns(ns);
}

// Note: this is an regression test
TEST(NodeTest, handlers_of_deleted_nodes_should_not_be_called_and_cause_no_exception) {  //NOLINT
  // GIVEN
  const auto kChannelName = "dynamic_allocated_node_test_channel";
  const auto kPort = 8765;
  auto io_service = std::make_shared<asio::io_service>();
  auto sender_node = pulicast::AsioNode("dynamic_allocated_node_test_sender", kPort, 0,
                                        10ms, io_service);
  auto &send_channel = sender_node[kChannelName];
  auto mock_subscription = testing::MockFunction<void(const std::string &)>();

  {
    // GIVEN
    auto receiver_node = pulicast::AsioNode(
        "dynamic_allocated_node_test_receiver", kPort, 0, 10ms, io_service);
    auto &receive_channel = receiver_node[kChannelName];
    receive_channel.Subscribe<std::string>(mock_subscription.AsStdFunction());

    // THEN
    EXPECT_CALL(mock_subscription, Call("message 1, shall be ok")).Times(1);

    // WHEN
    send_channel << "message 1, shall be ok";
    io_service->run_for(100ms);
    // Note: the receiver_node is deleted here and therefore the mock_subscription should not be
    // called any more
  }

  // WHEN
  std::cerr << "deleted receiver" << std::endl;
  send_channel << "message 2, shall be ignored and not lead to an error";

  // THEN
  EXPECT_NO_THROW(io_service->run_for(100ms));
}
