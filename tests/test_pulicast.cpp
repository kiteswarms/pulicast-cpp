/*     Copyright (C) 2021 Kiteswarms Ltd
 *
 *     This file is part of pulicast.
 *
 *     pulicast is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     pulicast is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with pulicast.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <chrono>
#include <thread>

#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <pulicast/pulicast.h>

using namespace std::chrono_literals;

/**
 * @brief Equality Matcher for Pulicast Node Messages
 *
 * This matcher checks the equality between two pulicast_message::pulicast_node messages.
 * First the name session_id and number of channels are checked. If they are the same the channel
 * info for each channel is checked.
 */
MATCHER_P(PulicastNodeMessageEQ, other, "Equality matcher for pulicast_node message") {
  // First check meta data
  if (arg.name != other.name || arg.session_id != other.session_id ||
      arg.num_channels != other.num_channels) {
    *result_listener << "\nNode Message not equal.\n"
                     << "Is:\n"
                     << "Name:\t" << arg.name
                     << "\nSession ID:\t" << arg.session_id
                     << "\nNum Channels:\t" << arg.num_channels
                     << "\n\nShould be:"
                     << "\nName:\t" << other.name
                     << "\nSession ID:\t" << other.session_id
                     << "\nNum Channels:\t" << other.num_channels;
    return false;
  }
  // Check channels (num channels are equal)
  for (size_t channel_num = 0; channel_num < arg.num_channels; channel_num++) {
    auto arg_channel = arg.channels[channel_num];
    auto other_channel = other.channels[channel_num];
    if (arg_channel.name != other_channel.name ||
        arg_channel.publishing_period_ns != other_channel.publishing_period_ns ||
        arg_channel.is_published != other_channel.is_published ||
        arg_channel.is_subscribed != other_channel.is_subscribed) {
      *result_listener << "\nChannel Info with Number " << channel_num << " is not equal!\n"
                       << "Is:"
                       << "\nName:\t" << arg_channel.name << "\nPublishing Periode:\t"
                       << arg_channel.publishing_period_ns << "\nIs Published:\t"
                       << std::to_string(arg_channel.is_published) << "\nIs Subscribed:\t"
                       << std::to_string(arg_channel.is_subscribed) << "\n\nShould be:"
                       << "\nName:\t" << other_channel.name << "\nPublishing Periode:\t"
                       << other_channel.publishing_period_ns << "\nIs Published:\t"
                       << std::to_string(other_channel.is_published) << "\nIs Subscribed:\t"
                       << std::to_string(other_channel.is_subscribed);
      return false;
    }
  }

  return true;
}


TEST(PulicastTest, AsioNodeTestInt) { //NOLINT
  int test_value = 5;
  testing::MockFunction<std::function<void(const int &)>> mock_callback;

  auto asio_node = pulicast::AsioNode("AsioNode");
  asio_node["subscription"].Subscribe<int>(mock_callback.AsStdFunction());
  asio_node["subscription"].SubscribeOrdered<int>(mock_callback.AsStdFunction());
  EXPECT_TRUE(asio_node["subscription"].IsSubscribed());

  EXPECT_CALL(mock_callback, Call(test_value)).Times(2);
  asio_node["subscription"] << test_value;
  asio_node.GetService()->poll();
}
