/*     Copyright (C) 2021 Kiteswarms Ltd
 *
 *     This file is part of pulicast.
 *
 *     pulicast is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     pulicast is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with pulicast.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <unordered_set>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <pulicast/core/channel_store.h>

#include "utils/mock_transports.h"

using namespace testing;

TEST(ChannelStoreTest, only_lazy_created_channel_should_be_found) {  // NOLINT
  // GIVEN
  auto cm = pulicast::core::ChannelStore(std::make_unique<pulicast::testutils::MockTransport>());

  // WHEN
  cm.GetChannel("TestChannel");

  // THEN
  EXPECT_NE(cm.find("TestChannel"), cm.end());
  EXPECT_EQ(cm.find("NotUsedChannel"), cm.end());
}

TEST(ChannelStoreTest, iterator_should_contain_all_channels) {  // NOLINT
  // GIVEN
  auto cm = pulicast::core::ChannelStore(std::make_unique<pulicast::testutils::MockTransport>());
  cm.GetChannel("Ch1");
  cm.GetChannel("Ch2");
  cm.GetChannel("Ch3");

  // WHEN
  auto channel_names = std::set<std::string>();
  for(auto& [name, ch] : cm) {
    channel_names.insert(name);
  }

  // THEN
  EXPECT_THAT(channel_names, ContainerEq(std::set<std::string>{"Ch1", "Ch2", "Ch3"}));
}



TEST(ChannelStoreTest, channel_added_cb_should_be_called_for_each_added_channel) {  // NOLINT
  // GIVEN
  auto cm = pulicast::core::ChannelStore(std::make_unique<pulicast::testutils::MockTransport>());

  auto channel_added_cb = testing::MockFunction<void(const std::string&)>();
  cm.on_channel_added_callback_ = channel_added_cb.AsStdFunction();

  // THEN
  {
    InSequence seq;
    EXPECT_CALL(channel_added_cb, Call("Ch1")).Times(Exactly(1));
    EXPECT_CALL(channel_added_cb, Call("Ch2")).Times(Exactly(1));
    EXPECT_CALL(channel_added_cb, Call("Ch3")).Times(Exactly(1));
  }

  // WHEN
  cm.GetChannel("Ch1");
  cm.GetChannel("Ch2");
  cm.GetChannel("Ch3");
  cm.GetChannel("Ch3");  // This one will not trigger another call to ChannelAdded
}