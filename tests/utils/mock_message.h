/*     Copyright (C) 2021 Kiteswarms Ltd
 *
 *     This file is part of pulicast.
 *
 *     pulicast is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     pulicast is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with pulicast.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <gmock/gmock.h>
#include <gtest/gtest.h>

namespace pulicast::testutils {
class MessageMock {
 public:
  int decode(const void* buffer, uint32_t offset, uint32_t maxlen) { return 0; }
  int encode(void* buf, uint32_t offset, uint32_t maxlen) { return 0; }
  uint32_t getEncodedSize() { return 1; }
  // MOCK_METHOD(int, decode, (const void*, uint32_t, uint32_t));
};

}  // namespace pulicast::testutils
