/*     Copyright (C) 2021 Kiteswarms Ltd
 *
 *     This file is part of pulicast.
 *
 *     pulicast is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     pulicast is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with pulicast.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef TESTS_UTILS_MOCK_NODE_H_
#define TESTS_UTILS_MOCK_NODE_H_

#include <chrono>
#include <pulicast/node.h>
#include "mock_transports.h"

using namespace std::chrono_literals;

namespace pulicast::testutils {
class MockNode : public pulicast::Node {
 public:
  MockNode(const std::string &name = "mono", PublishingPeriod announcement_period_ns = 100ms) :
      pulicast::Node(name, "/", announcement_period_ns,
                     std::unique_ptr<MockTransport>(InitTransport()))
                     {}

  MockNode(const std::string &name, const std::string& default_namespace, PublishingPeriod announcement_period_ns = 100ms) :
      pulicast::Node(name, default_namespace, announcement_period_ns,
                     std::unique_ptr<MockTransport>(InitTransport()))
  {}


  MockChanelSender& GetChannelSender(const std::string &channel_name) {
    this->GetChannel(channel_name); // ensure the channel exists
    return transport_->GetChannelSender(channel_name);
  }

  MockTransport& GetTransport() {
    return *transport_;
  }

 private:
  MockTransport* InitTransport() {
    transport_ = new MockTransport();
    return transport_;
  }

  MockTransport* transport_;
};
}

#endif //TESTS_UTILS_MOCK_NODE_H_
