/*     Copyright (C) 2021 Kiteswarms Ltd
 *
 *     This file is part of pulicast.
 *
 *     pulicast is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     pulicast is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with pulicast.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PULICAST_TOOLS_PULILOG_LOGGER_H_
#define PULICAST_TOOLS_PULILOG_LOGGER_H_

#include <string>
#include <zcm/zcm-cpp.hpp>

class Logger {
 public:
  Logger(const std::string &path, pulicast::AsioNode *node);

  void EnsureChannelIsLogged(const std::string &channel_name);

  void Close() {
    log_file_.close();
  }

 private:
  zcm::LogFile log_file_;
  pulicast::AsioNode *node_;
  int64_t event_num_ = 0;

  void LogChannel(const std::string &channel_name);

  static int64_t MicrosecondsSinceEpoch();
};

#endif //PULICAST_TOOLS_PULILOG_LOGGER_H_
