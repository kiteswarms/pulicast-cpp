/*     Copyright (C) 2021 Kiteswarms Ltd
 *
 *     This file is part of pulicast.
 *
 *     pulicast is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     pulicast is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with pulicast.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <string>
#include <unordered_set>
#include <chrono>

#include <zcm/eventlog.h>
#include <boost/program_options.hpp>
#include <pulicast/pulicast.h>
#include <asio/signal_set.hpp>
#include <iomanip>

#include "logger.h"

namespace po = boost::program_options;

int main(int argc, char **argv) {
  po::options_description desc("Write pulicast messages to a logfile in the ZCM log format.");

  std::vector<std::string> channels_to_log;
  desc.add_options()
      ("help,h", "produce help message")
      ("port,p",
       po::value<uint16_t>()->default_value(pulicast::DEFAULT_PORT),
       "The pulicast port to listen on")
      ("puli-interface",
       po::value<std::string>(),
       "The address of the network interface on which to send and receive messages.")
      ("output,o", po::value<std::string>()->default_value("pulilog"), "The log file to write to")
      ("timestamp_fn,t", "Add timestamp according to the output filename")
      ("puli-channel,i",
       po::value<std::vector<std::string> >(&channels_to_log),
       "A pulicast channel to log. Logs all channels if not specified.")
      ("suppress-metadata,m",
       "Whether to suppress logging the metadata published on the '__nodes' channel");

  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);

  if (vm.count("help")) {
    std::cout << desc << "\n";
    return 1;
  }

  auto output_filename = std::stringstream();
  output_filename << vm["output"].as<std::string>();
  if (vm.count("timestamp_fn")) {
    std::time_t t = std::time(nullptr);
    std::tm tm = *std::localtime(&t);
    output_filename << "_" << std::put_time(&tm, "%Y-%m-%d_%H:%M:%S") << ".log";
  }

  auto io_service = std::make_shared<asio::io_service>();
  pulicast::AsioNode node("logger", vm["port"].as<uint16_t>(), 0, 100ms, io_service,
                          vm.count("puli-interface") ?
                          std::make_optional(vm["puli-interface"].as<std::string>())
                                                     : std::nullopt);

  Logger logger(output_filename.str(), &node);

  if (vm.count("suppress-metadata") == 0)
    logger.EnsureChannelIsLogged("__nodes");

  if (channels_to_log.empty()) {
    node["__nodes"].Subscribe<pulicast_messages::pulicast_node>([&](const pulicast_messages::pulicast_node &node_info) {
      for (const auto &channel_info : node_info.channels) {
        // Node: this logs the channel no matter whether is is published or subscribed
        //  we risk logging 'stale' channels here but in turn we won't miss the first couple of
        //  messages on a newly published channel
        logger.EnsureChannelIsLogged(channel_info.name);
      }
    });
  } else {
    for (const auto &channel_name : channels_to_log) {
      logger.EnsureChannelIsLogged(channel_name);
    }
  }

  asio::signal_set signals(*io_service, SIGINT);

  signals.async_wait([&](const asio::error_code &error, int signal_number) {
    std::cout << "Quit " << std::endl;
    logger.Close();
    exit(0);
  });

  io_service->run();

  std::cout << "quitting" << std::endl;
}
