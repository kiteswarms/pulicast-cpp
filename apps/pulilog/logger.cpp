/*     Copyright (C) 2021 Kiteswarms Ltd
 *
 *     This file is part of pulicast.
 *
 *     pulicast is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     pulicast is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with pulicast.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <chrono>
#include <iostream>

#include <pulicast/pulicast.h>

#include "logger.h"

Logger::Logger(const std::string &path, pulicast::AsioNode *node) :
    log_file_(path, "w"),
    node_(node) {
  assert(log_file_.good());
}

void Logger::EnsureChannelIsLogged(const std::string &channel_name) {
  if (!node_->GetChannel(channel_name).IsSubscribed())
    LogChannel(channel_name);
}


void Logger::LogChannel(const std::string &channel_name) {
  std::cout << "Logging " << channel_name << std::endl;
  node_->GetChannel(channel_name).Subscribe([this, channel_name](const char *data, size_t size, const pulicast::MessageInfo &) {
    zcm::LogEvent event{event_num_,
                        MicrosecondsSinceEpoch(),
                        channel_name,
                        static_cast<int32_t>(size),
                        (uint8_t *) data};
    event_num_++;
    log_file_.writeEvent(&event);
  });
}

int64_t Logger::MicrosecondsSinceEpoch() {
  auto now = std::chrono::system_clock::now();
  return std::chrono::duration_cast<std::chrono::microseconds>(now.time_since_epoch()).count();
}


