/*     Copyright (C) 2021 Kiteswarms Ltd
 *
 *     This file is part of pulicast.
 *
 *     pulicast is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     pulicast is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with pulicast.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <asio.hpp>
#include <pulicast/pulicast.h>
#include <zcm/zcm-cpp.hpp>
#include <boost/program_options.hpp>

using namespace std::chrono_literals;
namespace po = boost::program_options;

int main(int argc, char **argv) {

  po::options_description desc("Replays pulicast messages from a logfile in the ZCM log format.");

  std::vector<std::string> channels_to_include;
  std::vector<std::string> channels_to_ignore_list;
  desc.add_options()
      ("help,h", "display this message")
      ("port,p", po::value<uint16_t>()->default_value(pulicast::DEFAULT_PORT),
       "The pulicast port to send on")
      ("puli-interface", po::value<std::string>(),
       "The address of the network interface on which to send and receive messages.")
      ("input,f", po::value<std::string>(), "The dump to replay")
      ("include-channel,i", po::value(&channels_to_include),
       "A pulicast channel to replay. Replays all channels if not specified.")
      ("exclude-channel,e", po::value(&channels_to_ignore_list),
          "A pulicast channel to replay. Replays all channels if not specified.");

  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);

  if (vm.count("help") || !vm.count("input")) {
    std::cout << desc << "\n";
    return 1;
  }

  auto should_publish_on_channel =
      [includes=std::set(channels_to_include.begin(), channels_to_include.end()),
      excludes = std::set(channels_to_ignore_list.begin(), channels_to_ignore_list.end())]
      (const std::string& channel_name) {
    auto is_in_includes = includes.empty() or includes.count(channel_name);
    auto is_in_excludes = excludes.count(channel_name);
    return is_in_includes and not is_in_excludes;
  };

  auto io_service = std::make_shared<asio::io_service>();
  auto node = pulicast::AsioNode("logger", vm["port"].as<uint16_t>(), 0, 100ms, io_service,
                                 vm.count("puli-interface") ?
                                 std::make_optional(vm["puli-interface"].as<std::string>())
                                                            : std::nullopt);

  auto publish_event = [&](const zcm::LogEvent &event) {
    node[event.channel].Publish((char *) event.data, event.datalen);
  };

  auto logfile = zcm::LogFile(vm["input"].as<std::string>(), "r");
  if (!logfile.good()) {
    std::cout << "Bad logfile: '" << vm["input"].as<std::string>() << "'!" << std::endl;
    return 1;
  }

  const zcm::LogEvent *evt = logfile.readNextEvent();
  if (!evt) {
    std::cout << "Empty logfile: '" << vm["input"].as<std::string>() << "'!" << std::endl;
    return 1;
  }

  auto first_timestamp = std::chrono::microseconds(evt->timestamp);
  auto start_time = std::chrono::system_clock::now();

  auto timer = asio::basic_waitable_timer<std::chrono::system_clock>(*io_service, 0s);

  std::function<void(const asio::error_code &)> send_one_event_and_schedule_next =
      [&](const asio::error_code &ec) {
        if (!ec) {
          if (should_publish_on_channel(evt->channel)) { publish_event(*evt); }

          evt = logfile.readNextEvent();
          if (evt) {
            auto duration_since_log_start = std::chrono::microseconds(evt->timestamp) - first_timestamp;
            timer.expires_at(start_time + duration_since_log_start);
            timer.async_wait(send_one_event_and_schedule_next);
          } else {
            io_service->stop();
          }
        }
      };

  timer.async_wait(send_one_event_and_schedule_next);

  io_service->run();
}