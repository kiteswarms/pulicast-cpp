/*     Copyright (C) 2021 Kiteswarms Ltd
 *
 *     This file is part of pulicast.
 *
 *     pulicast is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     pulicast is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with pulicast.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <fstream>
#include <iostream>
#include <chrono>

#include <asio.hpp>
#include <boost/program_options.hpp>
#include <pulicast_messages/ping.hpp>

#include <pulicast/pulicast.h>

namespace po = boost::program_options;

inline int64_t GetNanosecondsSinceUnixEpoch() {
  return std::chrono::duration_cast<std::chrono::nanoseconds>(
      std::chrono::system_clock::now().time_since_epoch()).count();
}

/**
   * @brief Publishes ping messages in regular intervals.
   *
   * The messages are timstamped_vector_uint messages where the first element of the vector contains
   * a sequence number (to later match ping messages to pong answers) and the rest of the entries is
   * filled up zeros to match the desired number of dummy bytes.
   */
class LoggedPingPublisher {

 public:
  LoggedPingPublisher(asio::io_service &service,
                      pulicast::Channel &ping_channel,
                      size_t num_dummy_bytes,
                      double interval_s, double startup_delay_s, const std::string &log_file_name)
      : timer(service),
        interval(interval_s),
        ping_channel(ping_channel),
        message_counter(0),
        num_dummy_bytes(num_dummy_bytes),
        log_file(log_file_name) {
    log_file << "ts,seq_no" << std::endl;
    timer.expires_after(
        std::chrono::duration_cast<decltype(timer)::duration>(
            std::chrono::duration<double>(startup_delay_s)));
    ArmTimer();
  }

  void PublishPing() {
    auto msg = MakeNextPingMessage();
    ping_channel << msg;
    log_file << msg.timestamp_ns << "," << msg.sequence_nr << std::endl;
  }

 private:
  asio::basic_waitable_timer<std::chrono::system_clock> timer;
  std::chrono::duration<double> interval;
  pulicast::Channel &ping_channel;
  uint64_t message_counter;
  size_t num_dummy_bytes;
  std::ofstream log_file;

  pulicast_messages::ping MakeNextPingMessage() {
    auto msg = pulicast_messages::ping();
    msg.timestamp_ns = GetNanosecondsSinceUnixEpoch();
    msg.sequence_nr = message_counter++;
    msg.payload = std::vector<uint8_t>(num_dummy_bytes, 0);
    msg.payload_size = msg.payload.size();
    return msg;
  }

  void ArmTimer() {
    timer.expires_at(std::chrono::time_point_cast<typename decltype(timer)::duration>(
        timer.expires_at() + interval));
    timer.async_wait([this](const asio::error_code &ec) {
      PublishPing();
      ArmTimer();
    });
  }
};

/**
 * @brief Subscribes to pong messages and writes their arrival time together with the sequence
 * number to a log file.
 */
class LoggedPongSubscriber {
 public:
  LoggedPongSubscriber(pulicast::Channel &pong_channel, const std::string &log_file_name) :
      log_file(log_file_name) {

    log_file << "received_ts,seq_no" << std::endl;

    pong_channel.Subscribe<pulicast_messages::ping>([this](const pulicast_messages::ping &msg) {
      auto now = GetNanosecondsSinceUnixEpoch();
      log_file << now << "," << msg.sequence_nr << std::endl;
    });
  }

 private:
  std::ofstream log_file;
};

/**
 * @brief Sends out pings in regular intervals and records when the pong answers come in.
 */
class NetworkProbe {
 public:
  NetworkProbe(pulicast::AsioNode *node,
               const std::string &probe_prefix,
               size_t num_dummy_bytes,
               double interval_s, double startup_delay_s) :
      publisher(*node->GetService(),
                node->GetChannel(probe_prefix + "ping"),
                num_dummy_bytes,
                interval_s,
                startup_delay_s,
                probe_prefix + "_pings.csv"),
      subscriber(node->GetChannel(probe_prefix + "pong"), probe_prefix + "_pongs.csv") {}
 private:
  LoggedPingPublisher publisher;
  LoggedPongSubscriber subscriber;
};

/**
 * @brief Echos back the ping messages of a NetworkProbe on a pong channel.
 */
class PingReflector {
 public:
  PingReflector(pulicast::AsioNode *node, const std::string &probe_prefix) :
      ping_channel(node->GetChannel(probe_prefix + "ping")),
      pong_channel(node->GetChannel(probe_prefix + "pong")) {
    ping_channel.Subscribe<pulicast_messages::ping>(
        [&](const pulicast_messages::ping &msg) {
          auto echo = pulicast_messages::ping();
          echo.timestamp_ns = GetNanosecondsSinceUnixEpoch();
          echo.sequence_nr = msg.sequence_nr;
          echo.payload_size = 0;
          pong_channel << echo;
        });
  }
 private:
  pulicast::Channel &ping_channel;
  pulicast::Channel &pong_channel;
};

int main(int argc, char **argv) {
  po::options_description desc
      ("Send ping messages in regular intervals and reply to ping messages with pong messages. "
       "Ping messages are sent and received by named probes. "
       "Pong messages are only reflected for the specified probes.");

  auto probe_names = std::vector<std::string>();
  auto reflector_names = std::vector<std::string>();
  auto port = uint16_t{};
  auto ttl = uint16_t{};
  auto num_payload_bytes = uint16_t{};
  auto interval = double{};
  auto startup_delay = double{};
  desc.add_options()
      ("help,h", "produce help message.")
      ("puli-port,p",
       po::value(&port)->default_value(pulicast::DEFAULT_PORT),
       "The pulicast port to listen on.")
      ("puli-ttl,t", po::value(&ttl)->default_value(0), "The pulicast ttl to publish.")
      ("puli-interface",
       po::value<std::string>(),
       "The address of the network interface on which to send and receive messages.")
      ("probe,i",
       po::value(&probe_names),
       "The name under which to probe for echos to ping messages.")
      ("reflected-probe,r",
       po::value(&reflector_names),
       "The name of a probe for which to echo ping messages to answer to.")
      ("payload-bytes,b",
       po::value(&num_payload_bytes)->default_value(0),
       "The number of dummy payload bytes to send along the ping message.")
      ("ping-interval,g",
       po::value(&interval)->default_value(0.1),
       "The interval at which ping messages are to be sent (in seconds).")
      ("startup-delay,d", po::value(&startup_delay)->default_value(0.2),
       "The time to wait before publishing pings (in seconds). "
       "This prevents accidental message loss because reflectors are still booting up.");

  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);

  if (vm.count("help")) {
    std::cout << desc << "\n";
    return 1;
  }

  auto io_service = std::make_shared<asio::io_service>();
  pulicast::AsioNode node("puliping", port, static_cast<uint8_t>(ttl), 100ms, io_service,
                          vm.count("puli-interface") ?
                          std::make_optional(vm["puli-interface"].as<std::string>())
                                                     : std::nullopt);

  // Note: we use a list of unique ptrs instead of list of NetworkProbe since this caused trouble
  // with the asio timer not being moveable. Unsure why though.
  auto probes = std::vector<std::unique_ptr<NetworkProbe> >();
  for (auto &probe_name : probe_names) {
    probes.emplace_back(
        std::make_unique<NetworkProbe>(&node,
                                       probe_name,
                                       num_payload_bytes,
                                       interval,
                                       startup_delay));
  }

  auto reflectors = std::vector<PingReflector>();
  for (auto &reflector_name : reflector_names) {
    reflectors.emplace_back(&node, reflector_name);
  }

  io_service->run();
}
