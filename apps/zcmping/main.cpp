/*     Copyright (C) 2021 Kiteswarms Ltd
 *
 *     This file is part of pulicast.
 *
 *     pulicast is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     pulicast is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with pulicast.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <fstream>
#include <iostream>
#include <thread>
#include <chrono>

#include <boost/program_options.hpp>
#include <zcm/zcm-cpp.hpp>
#include <pulicast_messages/ping.hpp>

namespace po = boost::program_options;

inline int64_t GetNanosecondsSinceUnixEpoch() {
  return std::chrono::duration_cast<std::chrono::nanoseconds>(
      std::chrono::system_clock::now().time_since_epoch()).count();
}

class LoggedPingPublisher {
 public:
  LoggedPingPublisher(zcm::ZCM *zcm_handle, const std::string &ping_channel, size_t num_dummy_bytes,
                      double interval_s, double startup_delay, const std::string &log_file_name) :
      zcm_handle(zcm_handle),
      interval_ns(interval_s * 1e9),
      ping_channel(ping_channel),
      message_counter(0),
      num_dummy_bytes(num_dummy_bytes),
      log_file(log_file_name) {
    log_file << "ts,seq_no" << std::endl;
    run_thread = std::thread([=]() {
      auto next_wakeup =
          std::chrono::system_clock::now() + std::chrono::nanoseconds((long) (startup_delay * 1e9));
      while (1) {
        std::this_thread::sleep_until(next_wakeup);
        PublishPing();
        next_wakeup += std::chrono::nanoseconds(interval_ns);
      }
    });
  }

  void PublishPing() {
    auto msg = MakeNextPingMessage();
    zcm_handle->publish(ping_channel, &msg);
    log_file << msg.timestamp_ns << "," << msg.sequence_nr << std::endl;
  }

 private:
  zcm::ZCM *zcm_handle;
  long interval_ns;
  const std::string ping_channel;
  uint64_t message_counter;
  size_t num_dummy_bytes;
  std::ofstream log_file;
  std::thread run_thread;

  pulicast_messages::ping MakeNextPingMessage() {
    auto msg = pulicast_messages::ping();
    msg.timestamp_ns = GetNanosecondsSinceUnixEpoch();
    msg.sequence_nr = message_counter++;
    msg.payload = std::vector<uint8_t>(num_dummy_bytes, 0);
    msg.payload_size = msg.payload.size();
    return msg;
  }
};

class LoggedPongSubscriber {
 public:
  LoggedPongSubscriber(zcm::ZCM *zcm_handle,
                       const std::string &pong_channel,
                       const std::string &log_file_name) :
      log_file(log_file_name) {

    log_file << "received_ts,seq_no" << std::endl;

    zcm_handle->subscribe<pulicast_messages::ping>(
        pong_channel,
        [&](const zcm::ReceiveBuffer *rb,
            const std::string &channel,
            const pulicast_messages::ping *msg) {
          auto now = GetNanosecondsSinceUnixEpoch();
          log_file << now << "," << msg->sequence_nr << std::endl;
        });
  }

 private:
  std::ofstream log_file;
};

/**
 * @brief Sends out pings in regular intervals and records when the pong answers come in.
 */
class NetworkProbe {
 public:
  NetworkProbe(zcm::ZCM *zcm_handle,
               const std::string &probe_prefix,
               size_t num_dummy_bytes,
               double interval_s,
               double startup_delay) :
      publisher(zcm_handle,
                probe_prefix + "ping",
                num_dummy_bytes,
                interval_s,
                startup_delay,
                probe_prefix + "_pings.csv"),
      subscriber(zcm_handle, probe_prefix + "pong", probe_prefix + "_pongs.csv") {}
 private:
  LoggedPingPublisher publisher;
  LoggedPongSubscriber subscriber;
};

/**
 * @brief Echos back the ping messages of a NetworkProbe on a pong channel.
 */
class PingReflector {
 public:
  PingReflector(zcm::ZCM *zcm_handle, const std::string &probe_prefix) :
      ping_channel(probe_prefix + "ping"),
      pong_channel(probe_prefix + "pong") {

    zcm_handle->subscribe<pulicast_messages::ping>(
        ping_channel,
        [=](const zcm::ReceiveBuffer *rbuf,
            const std::string &channel,
            const pulicast_messages::ping *msg) {
          auto echo = pulicast_messages::ping();
          echo.timestamp_ns = GetNanosecondsSinceUnixEpoch();
          echo.sequence_nr = msg->sequence_nr;
          echo.payload_size = 0;
          zcm_handle->publish(pong_channel, &echo);
        });
  }
 private:
  const std::string ping_channel;
  const std::string pong_channel;
};

int main(int argc, char **argv) {
  po::options_description desc
      ("Send ping messages in regular intervals and reply to ping messages with pong messages. "
       "Ping messages are sent and received by named probes. "
       "Pong messages are only reflected for the specified probes.");

  auto probe_names = std::vector<std::string>();
  auto reflector_names = std::vector<std::string>();
  auto url = std::string{};
  auto num_payload_bytes = uint16_t{};
  auto interval = double{};
  auto startup_delay = double{};
  desc.add_options()
      ("help,h", "produce help message.")
      ("url,u", po::value(&url)->default_value(""), "The ZCM URL to use.")
      ("probe,i",
       po::value(&probe_names),
       "The name under which to probe for echos to ping messages.")
      ("reflected-probe,r",
       po::value(&reflector_names),
       "The name of a probe for which to echo ping messages to answer to.")
      ("payload-bytes,b",
       po::value(&num_payload_bytes)->default_value(0),
       "The number of dummy payload bytes to send along the ping message.")
      ("ping-interval,t",
       po::value(&interval)->default_value(0.1),
       "The interval at which ping messages are to be sent (in seconds).")
      ("startup-delay,d", po::value(&startup_delay)->default_value(0.2),
       "The time to wait before publishing pings (in seconds). "
       "This prevents accidental message loss because reflectors are still booting up.");

  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);

  if (vm.count("help")) {
    std::cout << desc << "\n";
    return 1;
  }

  auto zcm_handle = zcm::ZCM(url);

  // Note: we use a list of unique ptrs instead of NetworkProbe since otherwise we get a segfault
  // when writing to files. Unsure why but might be related to the fact that file streams might not
  // be moved?
  auto probes = std::vector<std::unique_ptr<NetworkProbe> >();
  for (auto &probe_name : probe_names) {
    probes.emplace_back(
        std::make_unique<NetworkProbe>(&zcm_handle,
                                       probe_name,
                                       num_payload_bytes,
                                       interval,
                                       startup_delay));
  }

  auto reflectors = std::vector<PingReflector>();
  for (auto &reflector_name : reflector_names) {
    reflectors.emplace_back(&zcm_handle, reflector_name);
  }

  zcm_handle.run();
}
