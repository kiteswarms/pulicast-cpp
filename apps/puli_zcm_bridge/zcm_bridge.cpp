/*     Copyright (C) 2021 Kiteswarms Ltd
 *
 *     This file is part of pulicast.
 *
 *     pulicast is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     pulicast is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with pulicast.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <memory>
#include <iostream>
#include <chrono>

#include "zcm_bridge.h"

using namespace std::chrono_literals;

ZcmBridge::ZcmBridge(uint16_t puli_port,
                     uint8_t puli_ttl,
                     std::optional<std::string> interface_address,
                     const std::string &zcm_url,
                     const std::string &puli_prefix,
                     const std::string &zcm_prefix,
                     const std::vector<std::string> &puli_channels,
                     const std::vector<std::string> &zcm_channels) :
    node("pulicast_zcm_bridge", puli_port, puli_ttl, 100ms, std::make_shared<asio::io_service>(), std::move(interface_address)),
    zcm(zcm_url), own_socket_id_{0, 0}, puli_prefix_(puli_prefix), zcm_prefix_(zcm_prefix) {


  // This is a hack to deduce the socket id of the bridge node (this node)
  // As soon as we know the own socket id we can ignore packets coming from that socket id to
  // prevent self-loops on the pulicast side of the bridge.
  node["__nodes"].Subscribe<pulicast_messages::pulicast_node>(
      [this]
          (const pulicast_messages::pulicast_node &node_info, const pulicast::MessageInfo &message_info) {
        if (node_info.session_id == node.GetSessionID()) own_socket_id_ = message_info.source;
      });

  // This subscription kills the application as soon as another pulicast bridge ist detected
  // since right now I am not sure what would happen if there was more than one.
  node["__nodes"].Subscribe<pulicast_messages::pulicast_node>(
      [this](const pulicast_messages::pulicast_node &node_info) {
        if (IsForeignBridge(node_info)) {
          std::cerr << "Warning there seems to be another pulicast_zcm_bridge in the network! "
                       "This can have undesired effects!" << std::endl;
          exit(1);
        }
      });

  if (puli_channels.empty()) {  // Auto forward all pulicast channels if none are specified
    std::cerr << "Warning: all channels used in Pulicast are forwarded to ZCM!" << std::endl;

    // This subscription auto-forwards channels from pulicast to zcm
    node["__nodes"].Subscribe<pulicast_messages::pulicast_node>([this]
                                                       (const pulicast_messages::pulicast_node &node_info,
                                                        const pulicast::MessageInfo &message_info) {
      if (IsForeignPacket(message_info)) {
        // Set up forwards for channels
        for (const auto &channel : node_info.channels) {
          if (!IsIgnoredChannel(channel) && !ForwardToZCMInitiated(channel)) {
            InitiateForwardToZCM(channel.name);
          }
        }
      }
    });
  } else {  // Only forward the specified pulicast channels
    for (const auto &puli_channel : puli_channels)
      InitiateForwardToZCM(puli_channel);
  }

  if (zcm_channels.empty()) {
    std::cerr << "Warning: no channels are forwarded from ZCM to Pulicast!" << std::endl;
  }

  for (const auto &zcm_channel : zcm_channels)
    InitiateForwardToPulicast(zcm_channel);

}

bool ZcmBridge::ForwardToZCMInitiated(const pulicast_messages::pulicast_channel &channel) const {
  return initiated_puli_to_zcm_channels_.count(channel.name) > 0;
}

bool ZcmBridge::IsForwardedToZCM(const pulicast_messages::pulicast_channel &channel) const {
  return IsForwardedToZCM(channel.name);
}

bool ZcmBridge::IsForwardedToZCM(const std::string &channel) const {
  return puli_to_zcm_channels_.count(channel) > 0;
}

void ZcmBridge::InitiateForwardToZCM(const std::string &channel) {
  node[channel].Subscribe(
      [channel, this](const char *data, size_t size, const pulicast::MessageInfo &message_info) {
        if (IsForeignPacket(message_info)) {
          NotifyForwardToZCM(channel);
          zcm.publish(zcm_prefix_ + channel, reinterpret_cast<const uint8_t *>(data), size);
        }
      });
  std::cout << "Initiating puli:" << channel << "\t>>\tZCM:" << zcm_prefix_ << channel << std::endl;
  initiated_puli_to_zcm_channels_.insert(channel);
}

void ZcmBridge::NotifyForwardToZCM(const std::string &channel) {
  if (!IsForwardedToZCM(channel)) {
    std::cout << "puli:" << channel << "\t>>\tZCM:" << zcm_prefix_ << channel << std::endl;
    puli_to_zcm_channels_.insert(channel);
    if (IsForwardedToPulicast(channel)) {
      WarnAboutBidirectionalChannel(channel);
    }
  }
}
void ZcmBridge::WarnAboutBidirectionalChannel(const std::string &channel) const {
  std::cerr << "Warning: " << channel << " is forwarded in both directions!" << std::endl;
}
bool ZcmBridge::IsForwardedToPulicast(const std::string &channel) const {
  return zcm_to_puli_channels_.count(channel) > 0;
}

bool ZcmBridge::IsIgnoredChannel(const pulicast_messages::pulicast_channel &channel) const {
  return channel.name == "__nodes";
}

bool ZcmBridge::IsForeignBridge(const pulicast_messages::pulicast_node &node_info) {
  return node_info.session_id != node.GetSessionID() && node_info.name == node.GetName();
}

void ZcmBridge::Run() {
  zcm.start();
  node.GetService()->run();
}

bool ZcmBridge::IsForeignPacket(const pulicast::MessageInfo &message_info) const {
  return message_info.source != own_socket_id_
      && own_socket_id_ != pulicast::Address{0, 0};
}

void ZcmBridge::InitiateForwardToPulicast(const std::string &channel) {
  zcm.subscribe(channel, &ZcmBridge::OnZCMMessage, this);
}

void ZcmBridge::OnZCMMessage(const zcm::ReceiveBuffer *buffer, const std::string &channel) {
  NotifyForwardToPulicast(channel);
  node[puli_prefix_ + channel].Publish(reinterpret_cast<const char *>(buffer->data),
                                       buffer->data_size);
}
void ZcmBridge::NotifyForwardToPulicast(const std::string &channel) {
  if (!IsForwardedToPulicast(channel)) {
    std::cout << "puli:" << puli_prefix_ << channel << "\t<<\tZCM:" << channel << std::endl;
    zcm_to_puli_channels_.insert(channel);
    if (IsForwardedToZCM(channel)) {
      WarnAboutBidirectionalChannel(channel);
    }
  }
}

// TODO: don't auto-prevent self loops but warn about them and disable the forward-all feature from zcm to pulicast
