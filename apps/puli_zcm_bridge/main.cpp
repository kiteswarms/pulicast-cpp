/*     Copyright (C) 2021 Kiteswarms Ltd
 *
 *     This file is part of pulicast.
 *
 *     pulicast is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     pulicast is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with pulicast.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <iostream>
#include <string>
#include <vector>
#include <optional>

#include <boost/program_options.hpp>

#include "zcm_bridge.h"

namespace po = boost::program_options;

int main(int argc, char **argv) {
  po::options_description desc("Forward messages from pulicast to ZCM and from ZCM to pulicast.");

  std::vector<std::string> puli_channels;
  std::vector<std::string> zcm_channels;
  desc.add_options()
      ("help,h", "produce help message")
      ("puli-port,p", po::value<int>()->default_value(pulicast::DEFAULT_PORT), "The pulicast port to publish on")
      ("puli-ttl,t", po::value<int>()->default_value(0), "The pulicast ttl to publish with")
      ("puli-interface", po::value<std::string>(), "The address of the network interface on which to send and receive messages.")
      ("zcm-url,u", po::value<std::string>()->default_value(""), "The ZCM URL to publish on")
      ("puli-prefix",
       po::value<std::string>()->default_value(""),
       "The prefix to put before channels published on pulicast")
      ("zcm-prefix",
       po::value<std::string>()->default_value(""),
       "The prefix to put before channels published on zcm")
      ("puli-channel,i",
       po::value<std::vector<std::string> >(&puli_channels),
       "A pulicast channel to forward to ZCM. Forwards all channels if not specified.")
      ("zcm-channel,z",
       po::value<std::vector<std::string> >(&zcm_channels),
       "A ZCM channel to forward to pulicast. Forwards NO channels if not specified.");

  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);

  if (vm.count("help")) {
    std::cout << desc << "\n";
    return 1;
  }

  ZcmBridge forwarder(vm["puli-port"].as<int>(),
                      vm["puli-ttl"].as<int>(),
                      vm.count("puli-interface") ? std::make_optional(vm["puli-interface"].as<std::string>()) : std::nullopt,
                      vm["zcm-url"].as<std::string>(),
                      vm["puli-prefix"].as<std::string>(),
                      vm["zcm-prefix"].as<std::string>(),
                      puli_channels,
                      zcm_channels);
  forwarder.Run();
}