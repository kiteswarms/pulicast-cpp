/*     Copyright (C) 2021 Kiteswarms Ltd
 *
 *     This file is part of pulicast.
 *
 *     pulicast is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     pulicast is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with pulicast.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PULICAST_TOOLS_ZCM_BRIDGE_ZCM_BRIDGE_H_
#define PULICAST_TOOLS_ZCM_BRIDGE_ZCM_BRIDGE_H_

#include <cstdint>
#include <vector>
#include <string>
#include <unordered_set>
#include <optional>

#include <asio.hpp>
#include <zcm/zcm-cpp.hpp>

#include <pulicast/pulicast.h>

class ZcmBridge {
 public:

  ZcmBridge(uint16_t puli_port,
            uint8_t puli_ttl,
            std::optional<std::string> interface_address,
            const std::string &zcm_url,
            const std::string &puli_prefix,
            const std::string &zcm_prefix,
            const std::vector<std::string> &puli_channels,
            const std::vector<std::string> &zcm_channels);
  void Run();

 private:
  std::shared_ptr<asio::io_service> service;
  pulicast::AsioNode node;
  zcm::ZCM zcm;
  pulicast::Address own_socket_id_;

  std::unordered_set<std::string> puli_to_zcm_channels_;
  std::unordered_set<std::string> initiated_puli_to_zcm_channels_;
  std::unordered_set<std::string> zcm_to_puli_channels_;
  std::unordered_set<std::string> initiated_zcm_to_puli_channels_;
  const std::string puli_prefix_;
  const std::string zcm_prefix_;

  void InitiateForwardToZCM(const std::string &channel);

  void InitiateForwardToPulicast(const std::string &channel);

  void OnZCMMessage(const zcm::ReceiveBuffer *buffer, const std::string &channel);
  bool IsForeignPacket(const pulicast::MessageInfo &message_info) const;
  bool IsForeignBridge(const pulicast_messages::pulicast_node &node_info);
  bool IsIgnoredChannel(const pulicast_messages::pulicast_channel &channel) const;
  bool IsForwardedToZCM(const pulicast_messages::pulicast_channel &channel) const;
  void NotifyForwardToZCM(const std::string &channel);
  bool IsForwardedToZCM(const std::string &channel) const;
  bool IsForwardedToPulicast(const std::string &channel) const;
  void WarnAboutBidirectionalChannel(const std::string &channel) const;
  void NotifyForwardToPulicast(const std::string &channel);
  bool ForwardToZCMInitiated(const pulicast_messages::pulicast_channel &channel) const;
};

#endif //PULICAST_TOOLS_ZCM_BRIDGE_ZCM_BRIDGE_H_
