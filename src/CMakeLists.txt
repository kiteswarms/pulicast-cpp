# Copied from https://gitlab.com/CLIUtils/modern-cmake/-/blob/master/examples/extended-project/src/CMakeLists.txt and edited for cookiecutter compatibility.

find_package(Threads)

# Note that headers are optional, and do not affect add_library, but they will not
# show up in IDEs unless they are listed in add_library.

include(message_generation.cmake)

# Optionally glob, but only for CMake 3.12 or later:
file(GLOB_RECURSE HEADER_LIST CONFIGURE_DEPENDS "${ModernCMakeExample_SOURCE_DIR}/include/*.h")

add_library(pulicast_asio INTERFACE ${HEADER_LIST})
add_dependencies(pulicast_asio pulicast_messages)
target_compile_definitions(pulicast_asio INTERFACE ASIO_DISABLE_THREADS)
add_library(pulicast::pulicast_asio ALIAS pulicast_asio)

add_library(pulicast_asio_multithreaded INTERFACE ${HEADER_LIST})
add_dependencies(pulicast_asio_multithreaded pulicast_messages)
add_library(pulicast::pulicast_asio_multithreaded ALIAS pulicast_asio_multithreaded)
target_link_libraries(pulicast_asio_multithreaded INTERFACE Threads::Threads)

add_library(pulicast INTERFACE ${HEADER_LIST})
add_dependencies(pulicast pulicast_messages)
add_library(pulicast::pulicast ALIAS pulicast)
target_link_libraries(pulicast INTERFACE Threads::Threads)


# We need this directory, and users of our library will need it too
target_include_directories(pulicast_asio
    INTERFACE 
        $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/include>
        $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}/include>
        $<INSTALL_INTERFACE:include>
)

target_include_directories(pulicast
        INTERFACE
        $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/include>
        $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}/include>
        $<INSTALL_INTERFACE:include>
        )


target_include_directories(pulicast_asio_multithreaded
        INTERFACE
        $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/include>
        $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}/include>
        $<INSTALL_INTERFACE:include>
        )


# This depends on (header only) boost
# target_link_libraries(modern_library PRIVATE Boost::boost)

# All users of this library will need at least C++17
target_compile_features(
    pulicast_asio
    INTERFACE
        cxx_std_17
)

target_compile_features(
        pulicast_asio_multithreaded
        INTERFACE
        cxx_std_17
)

target_compile_features(
        pulicast
        INTERFACE
        cxx_std_17
)

# IDEs should put the headers in a nice place
source_group(
    TREE "${PROJECT_SOURCE_DIR}/include"
    PREFIX "Header Files"
    FILES ${HEADER_LIST}
)

# 
# Deployment
# 

# Library
install(TARGETS pulicast
    EXPORT pulicast-targets
    COMPONENT pulicast-dev)

install(TARGETS pulicast_asio
    EXPORT pulicast-asio-targets
    COMPONENT pulicast-dev)

install(TARGETS pulicast_asio_multithreaded
    EXPORT pulicast-asio-multithreaded-targets
    COMPONENT pulicast-dev)

# Will include the top level include directory for the target
install(DIRECTORY ${PROJECT_SOURCE_DIR}/include/
    DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}
)

## Will include generated headers (optional, i.e. version header and export headers)
#install(DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/include/
#    DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}
#
install(EXPORT pulicast-targets
    FILE pulicast-targets.cmake
    NAMESPACE pulicast::
    DESTINATION ${INSTALL_CONFIGDIR}
)

install(EXPORT pulicast-asio-targets
    FILE pulicast-asio-targets.cmake
    NAMESPACE pulicast::
    DESTINATION ${INSTALL_CONFIGDIR}
)

install(EXPORT pulicast-asio-multithreaded-targets
        FILE pulicast-asio-multithreaded-targets.cmake
        NAMESPACE pulicast::
        DESTINATION ${INSTALL_CONFIGDIR}
        )
