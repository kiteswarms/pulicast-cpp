/*     Copyright (C) 2021 Kiteswarms Ltd
 *
 *     This file is part of pulicast.
 *
 *     pulicast is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     pulicast is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with pulicast.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef INCLUDE_PULICAST_NODE_H_
#define INCLUDE_PULICAST_NODE_H_

#include <string>
#include <memory>
#include <utility>

#include "pulicast_messages/pulicast_node.hpp"

#include "pulicast/channel.h"
#include "pulicast/core/channel_store.h"
#include "pulicast/core/utils/randomness.h"
#include "pulicast/namespace.h"

namespace pulicast {

/**
 * The node is the central handle to the pulicast API. It provides access to Channels for publishing
 * and subscribing and announces itself in the pulicast network to enable global discovery of other
 * nodes.
 *
 * @note Subclasses or direct creators of Node must ensure that the AnnounceNode() method is called
 * in periodic intervals according to the given announcement period.
 */
class Node : public Namespace {
 public:

  /**
   * @brief Create a new pulicast node.
   *
   * @note A pulicast node is at the same time a pulicast namespace.
   *
   * @param name The name under which the node should announce itself. Duplicate names are allowed.
   * @param default_namespace The namespace relative to which channels are to be created.
   * The new node is at the same time a namespace.
   * @param announcement_period_ns The time between announcements in nanoseconds or anything
   * implicitly convertible.
   * @param transport The transport to be used by the node and its channels.
   */
  Node(const std::string &name, const std::string &default_namespace,
       PublishingPeriod announcement_period_ns,
       std::unique_ptr<core::Transport> transport) :
  // HACK: here we pass an uninitialized reference to the Namespace constructor. This only works
  // since we know, that the Namespace constructor will just store the reference an not use it!
      Namespace(channel_store_, default_namespace),
      channel_store_(std::move(transport)),
      publishing_period_(announcement_period_ns),
      nodes_channel_(channel_store_.GetChannel("__nodes")){
    node_info_.name = name;
    node_info_.session_id = core::pulicast_rand();

    channel_store_.on_channel_added_callback_ = [this](const std::string &channel_name) {
      if (channel_name != "__nodes")
        this->AnnounceNode();
    };
  }

  Node(const Node &) = delete;
  Node &operator=(const Node &) = delete;
  Node(Node &&) = delete;
  Node &operator=(Node &&) = delete;
  virtual ~Node() =
  default;

  /**
   * @brief Makes a new namespace that refers to this node.
   *
   * The new namespace is **NOT** relative to the default namespace of the node!
   *
   * @param name_space The name of the namespace.
   * @return A new namespace, that refers to this node.
   */
  Namespace MakeNamespace(std::string name_space) {
    return Namespace(channel_store_, name_space);
  }

  /**
   * @brief Returns a channel with the given name relative to the namespace that this node
   * represents.
   *
   * @param name The channels name. Can include namespace hierarchy e.g. use `../ch_name` to access
   * a channel in a higher namespace.
   * @return A reference to the given channel.
   */
  Channel& GetChannel(const std::string &name) {
    return (*this)[name];
  }

  /**
   * @brief Announces the node in the pulicast network.
   *
   * Announces the node within the pulicast network by sending a heartbeat message with its name and
   * session id to the `__nodes` channel.
   */
  void AnnounceNode() {
    nodes_channel_.publishing_period_ = publishing_period_;
    node_info_.channels.clear();
    for (const auto &name_and_channel : channel_store_) {
      node_info_.channels.push_back(MakeChannelInfo(name_and_channel.second));
    }
    node_info_.num_channels = node_info_.channels.size();
    nodes_channel_ << node_info_;
  }

  /**
   * @return The randomly generated session ID of the node. It is used by the pulicast discovery
   * system to distinguish nodes with equal names and restarts of nodes.
   */
  int64_t GetSessionID() const {
    return node_info_.session_id;
  }

  /**
   * @return The nodes name.
   */
  const std::string GetName() const {
    return node_info_.name;
  }

  /**
   * @return The time between two node announcements.
   */
  PublishingPeriod GetAnnouncementPeriod() {
    return publishing_period_;
  }

 private:
  core::ChannelStore channel_store_;
  pulicast_messages::pulicast_node node_info_;
  PublishingPeriod publishing_period_;
  Channel& nodes_channel_;

  static pulicast_messages::pulicast_channel MakeChannelInfo(const Channel &channel) {
    pulicast_messages::pulicast_channel channel_info;
    channel_info.name = channel.GetName();
    channel_info.publishing_period_ns = channel.publishing_period_.count();
    channel_info.is_published = channel.IsPublished();
    channel_info.is_subscribed = channel.IsSubscribed();
    return channel_info;
  }

};

}  // namespace pulicast

#endif //INCLUDE_PULICAST_NODE_H_
