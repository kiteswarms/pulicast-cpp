/*     Copyright (C) 2021 Kiteswarms Ltd
 *
 *     This file is part of pulicast.
 *
 *     pulicast is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     pulicast is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with pulicast.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef INCLUDE_PULICAST_TRANSPORT_UDP_TRANSPORT_H_
#define INCLUDE_PULICAST_TRANSPORT_UDP_TRANSPORT_H_

#include <asio/buffer.hpp>

#include "pulicast/core/transport.h"
#include "pulicast/core/utils/extract_from_buffer.h"
#include "pulicast/core/utils/scatter_gather_buffer.h"

namespace pulicast::transport {

constexpr const auto kPacketHeader = std::string_view("PULI");

/**
 * @brief SDBM hashing function for strings.
 *
 * Note: its probably good enough for us. See
 * https://softwareengineering.stackexchange.com/questions/49550/which-hashing-algorithm-is-best-for-uniqueness-and-speed/145633#145633
 *
 * @param str Pointer to the start of the string to hash.
 * @param length The length of the string to hash.
 * @return The sdbm of the given string.
 */
inline uint32_t sdbm(const char *str, size_t length) {
  uint32_t hash = 0;
  for (size_t i = 0; i < length; ++str, ++i) {
    hash = (*str) + (hash << 6u) + (hash << 16u) - hash;
  }
  return hash;
}

/**
 * @brief Compute the multicast group on which a given channel is to be published.
 *
 * @param channel_name The name of the channel to publish.
 * @return The mutlicast group number. Note that only valid multicast groups will be returned.
 */
inline uint32_t MulticastGroupForChannel(std::string_view channel_name) {
  return (239u << 24u) + sdbm(channel_name.data(), channel_name.size()) % (1u << 23u);
}

/**
 * @brief Static polymorphism base class for a channel sender that sends channels on UDP multicast
 * groups.
 *
 * Read more about static polymorphism here:
 * https://en.wikipedia.org/wiki/Template_metaprogramming#Static_polymorphism
 *
 * @tparam SocketSender The class inheriting from this class. Must implement
 * `SendToSocket(core::ScatterGatherBuffer &data)` which just passes the the given data straight to
 * a socket sending on the multicast group as derived by `MulticastGroupForChannel`.
 */
template<class SocketSender>
class UDPChannelSender : public pulicast::core::ChannelSender {
 public:
  UDPChannelSender(const std::string &channel_name) :
      channel_name_(channel_name) {}

  /**
   * @brief Sends the given data with channel name and magic header prepended.
   * @param data The data to send.
   */
  void Send(core::ScatterGatherBuffer &data) override {
    auto data_with_channel_name = data.push_front({channel_name_.data(), channel_name_.size()});

    uint8_t channel_name_length = channel_name_.size();
    auto data_with_channel_name_length = data_with_channel_name.push_front({&channel_name_length, 1});
    auto data_with_packet_header = data_with_channel_name_length.push_front({kPacketHeader.data(), kPacketHeader.size()});
    static_cast<SocketSender *>(this)->SendToSocket(data_with_packet_header);
  }

 private:
  std::string channel_name_;
};

/**
 * @brief Extracts the channel name and header from a buffer.
 *
 * @param buffer The buffer to extract the data from. Its start and length will be modified during
 * extraction.
 * @return The channel name if it could be parsed or a std::nullopt if there was an error during parsing.
 * Note that if there was a parsing error, the state of the buffer is undefined and it should not be
 * used.
 */
inline std::optional<std::string_view> ExtractHeaderAndChannelName(asio::mutable_buffer& buffer) {
  using pulicast::core::ExtractStringView;
  using pulicast::core::Extract;

  auto magic = ExtractStringView(buffer, kPacketHeader.size());
  if (!magic.has_value() || (*magic) != kPacketHeader) return std::nullopt;

  auto channel_name_length = Extract<uint8_t>(buffer);
  if (!channel_name_length.has_value()) return std::nullopt;

  auto channel_name = ExtractStringView(buffer, *channel_name_length);
  return channel_name;
}

}  // namespace pulicast::transport

#endif //INCLUDE_PULICAST_TRANSPORT_UDP_TRANSPORT_H_
