/*     Copyright (C) 2021 Kiteswarms Ltd
 *
 *     This file is part of pulicast.
 *
 *     pulicast is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     pulicast is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with pulicast.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef INCLUDE_PULICAST_TRANSPORT_ASIO_TRANSPORT_H_
#define INCLUDE_PULICAST_TRANSPORT_ASIO_TRANSPORT_H_

#include <memory>
#include <utility>
#include <vector>
#include <string>

#include <asio/io_service.hpp>
#include <asio/buffer.hpp>
#include <asio/ip/udp.hpp>
#include <asio/ip/multicast.hpp>
#include <asio/io_context_strand.hpp>

#include "pulicast/address.h"
#include "pulicast/core/transport.h"
#include "pulicast/transport/udp_transport.h"
#include "pulicast/message_info.h"

namespace pulicast::transport {

/**
 * @brief The PeriodicTimer allows to execute a callback at fixed intervals.
 * @tparam ClockType The std::chrono clock type to use.
 * Right now there is just std::chrono::system_clock and std::chono::steady_clock
 * but in C++20 there will be more choice.
 */
template<class ClockType = std::chrono::system_clock>
class PeriodicTimer {
 public:

  /**
   * @brief Creates a new periodic timer based on a std::chono::duration as the interval.
   *
   * @tparam Rep derived from the passed interval.
   * @tparam Period derived from the passed interval.
   * @param interval The interval at which the callback should be called.
   * @param callback The callback to execute periodically.
   * @param io_service The io_service to use when constructing asio timers.
   */
  // This weird template construct allows us to pass any kind of chrono::duration
  template<typename Rep, typename Period>
  PeriodicTimer(std::chrono::duration<Rep, Period> interval,
                std::function<void(void)> callback,
                asio::io_service &io_service) :
      interval_(std::chrono::duration_cast<decltype(interval_)>(interval)),
      callback_(callback),
      asio_timer_(io_service) {}

  ~PeriodicTimer() {
    Stop();
  }

  /**
 * @brief Starts the periodic execution of the callback.
 */
  void Start() {
    *is_active_ = true;
    asio_timer_.expires_from_now(ClockType::duration::zero());
    KeepExecutingCallback();
  }

  /**
   * @brief Starts the periodic execution of the callback at a specific time.
   *
   * If the time is in the past the execution will start immediately.
   * Allows for synchronized starting of multiple timers.
   *
   * @param start_time The time at which to start the periodic execution.
   */
  void StartAt(typename ClockType::time_point start_time) {
    *is_active_ = true;
    asio_timer_.expires_at(start_time);
    KeepExecutingCallback();
  }

  /**
   * @brief Starts the periodic execution of the callback after a given delay.
   * @tparam Rep derived from the passed interval.
   * @tparam Period derived from the passed interval.
   * @param delay The duration to wait before starting the periodic execution.
   */
  template<typename Rep, typename Period>
  void StartAfter(std::chrono::duration<Rep, Period> delay) {
    *is_active_ = true;
    asio_timer_.expires_from_now(delay);
    KeepExecutingCallback();
  }

  /**
   * @brief Stop the periodic execution of the callback.
   */
  void Stop() {
    *is_active_ = false;
  }

 private:
  typename ClockType::duration interval_;
  std::function<void(void)> callback_;
  asio::basic_waitable_timer<ClockType> asio_timer_;
  std::shared_ptr<std::atomic<bool>> is_active_ = std::make_shared<std::atomic<bool>>(false);

  // Note: unfortunately the asio timers cancel() function is not robust.
  // There can be rare cases where the timer is canceled but its handler function has already been
  // scheduled to the asio event loop. Then the timers handler is called without an error code of
  // asio::error::operation_aborted even though the timer is already cancelled!
  // The usual way to deal with this issue is to use a sentinel value for the timer timeout.
  // See here for details: https://stackoverflow.com/a/43169596
  //
  // Since we want thread safety, we use an extra atomic bool (is_active_) to remember whether the
  // timer is active. Then we can check this flag in the asio timer handler and when it is set to
  // false we 1. abort calling the user callback and 2. stop rescheduling the timer.
  //
  // Now there can be the additional edge case, that the PeriodicTimer owning the asio timer has
  // been deleted. This will cause an invalid read to is_active_ in the asio timers handler!
  // Therefore we additionally wrap that flag in a shared pointer.
  // So even if the PeriodicTimer is deleted, its is_active_ flag stays in memory until the very
  // last execution of the asio timer handler.

  void KeepExecutingCallback() {
    // Note: in the line below `running = is_active_` copies the shared pointer to the is_active_
    // flag into the handler function to ensure that we can check the flag even when the surrounding
    // PeriodicTimer already has been deleted.
    auto asio_timer_handler = [this, running = is_active_](const asio::error_code &error) {
      if (error == asio::error::operation_aborted or not *running) {
        return;
      }
      callback_();
      asio_timer_.expires_at(asio_timer_.expires_at() + interval_);
      KeepExecutingCallback();
    };
    asio_timer_.async_wait(asio_timer_handler);
  }
};

class AsioChannelSender : public UDPChannelSender<AsioChannelSender> {
 public:
  AsioChannelSender(const std::string &channel_name,
                    uint8_t ttl,
                    uint16_t port,
                    asio::io_service &io_service, std::optional<asio::ip::address_v4> interface)
      : UDPChannelSender<AsioChannelSender>(channel_name),
        endpoint_(asio::ip::address_v4(MulticastGroupForChannel(channel_name)), port),
        socket_(io_service, asio::ip::udp::v4()) {

    // Set TTL
    socket_.set_option(asio::ip::multicast::hops(ttl));
    // TODO(max): aaahhhh wy do we need this? Only this guy knows:
    //  https://askubuntu.com/a/1243034/1180708
    if (ttl == 0) {
      socket_.set_option(
          asio::ip::multicast::join_group(
              asio::ip::address_v4(MulticastGroupForChannel(channel_name))));
    }

    // Set Loopback option
    socket_.set_option(asio::ip::multicast::enable_loopback(true));

    // Set Network Interface
    if (interface && *interface != asio::ip::address_v4::any()) {
      socket_.set_option(asio::ip::multicast::outbound_interface(*interface));
    }

  }

  void SendToSocket(const core::ScatterGatherBuffer &data) {
    socket_.send_to(data, endpoint_);  // TODO(max): here we just hope that this is thread-safe!
  }

 private:
  asio::ip::udp::endpoint endpoint_;
  asio::ip::udp::socket socket_;
};

class AsioChannelReader {
 public:
  AsioChannelReader(const std::string &channel_name,
                    std::function<void(asio::mutable_buffer &, Address)> cb,
                    asio::io_service &io_service,
                    uint16_t port,
                    std::optional<asio::ip::address_v4> interface) :
      channel_name_(channel_name),
      receive_socket_(io_service),
      cb_(std::move(cb)),
      channel_strand_(io_service) {

    // Create the receiving socket (as ipv4)
    auto endpoint = asio::ip::udp::endpoint(asio::ip::address_v4::any(), port);
    receive_socket_.open(endpoint.protocol());

    // Set Reuse Hints
    receive_socket_.set_option(asio::ip::udp::socket::reuse_address(true));

    // Prevent receiving packets from all joined MC groups of this host
    int mc_all = 0;
    setsockopt(receive_socket_.native_handle(),
               IPPROTO_IP,
               IP_MULTICAST_ALL,
               &mc_all,
               sizeof(mc_all));

    // Binding to any address and the pulicast port
    receive_socket_.bind(endpoint);

    // Join the multicast group on the socket
    receive_socket_.set_option(
        asio::ip::multicast::join_group(
            asio::ip::address_v4(MulticastGroupForChannel(channel_name)),
            interface ? *interface : asio::ip::address_v4::any()));

    if (interface &&
        *interface != asio::ip::address_v4::any() &&
        *interface != asio::ip::address_v4::loopback()) {
      receive_socket_.set_option(
          asio::ip::multicast::join_group(
              asio::ip::address_v4(MulticastGroupForChannel(channel_name)),
              asio::ip::address_v4::loopback()));
    }

    // prevents that we accidentally use blocking methods on the socket
    receive_socket_.non_blocking(true);

    // TODO(max): set receive buffer here? Ref issue #13
    DoRead();
  }

 private:
  void DoRead() {
    receive_socket_.async_wait(asio::ip::udp::socket::wait_read,
        channel_strand_.wrap([this](asio::error_code ec) {
          if (ec) {
            return;
          }

          // Note: in some strange cases when running the eventloop from many threads, this handler
          //   is executed even though available_bytes = 0. Therefore we have to check for this
          //   condition here, otherwise the receive_from call below would throw an exception.
          const size_t available_bytes = receive_socket_.available();
          if (available_bytes > 0) {
            auto buffer_store = std::vector<char>(available_bytes);
            auto buffer = asio::buffer(buffer_store);
            asio::ip::udp::endpoint source_addr;
            receive_socket_.receive_from(buffer, source_addr);

            auto channel_name = ExtractHeaderAndChannelName(buffer);
            if (channel_name.has_value() && channel_name == channel_name_) {
              cb_(buffer, Address{static_cast<uint32_t>(source_addr.address().to_v4().to_ulong()),
                                  source_addr.port()});
            }
          }
          DoRead();
        }));
  }

  std::string channel_name_;
  asio::ip::udp::socket receive_socket_;
  std::function<void(asio::mutable_buffer &, Address)> cb_;
  asio::io_service::strand channel_strand_;
};

class AsioTransport : public pulicast::core::Transport {
 public:

  AsioTransport(uint16_t port,
                uint8_t ttl,
                std::shared_ptr<asio::io_service> service,
                std::optional<std::string> interface_address = std::nullopt)
      : io_service_(std::move(service)),
        port_(port),
        ttl_(ttl),
        interface_address_(interface_address ?
                           std::make_optional(asio::ip::address_v4::from_string(*interface_address))
                                             : std::nullopt) {
    //TODO(max): warn if ttl=0 and interface address is neither empty nor loopback
  }

  std::unique_ptr<core::ChannelSender> MakeChannelSender(const std::string &channel) override {
    return std::make_unique<AsioChannelSender>(channel,
                                               ttl_,
                                               port_,
                                               *io_service_,
                                               interface_address_);
  }

  void StartReceivingMessagesFor(const std::string &channel,
                                 std::function<void(asio::mutable_buffer &, Address)> cb) override {
    channel_readers_.emplace_back(std::make_unique<AsioChannelReader>(channel,
                                                                      std::move(cb),
                                                                      *io_service_,
                                                                      port_, interface_address_));
  }

 private:
  // Note: It is important, that the io_service_ is the topmost member in this class to ensure that
  //   it gets deleted last. Otherwise we might get segfaults or memory leaks. This is due to the
  //   non-advised storage of the io_service inside a shared ptr.
  //   (see https://github.com/chriskohlhoff/asio/issues/16)
  //   also see issue 84
  std::shared_ptr<asio::io_service> io_service_;
  const uint16_t port_;
  uint8_t ttl_;
  std::optional<asio::ip::address_v4> interface_address_;
  std::vector<std::unique_ptr<AsioChannelReader>> channel_readers_;
};

/**
 * @brief Wraps a subscription handler into a strand.
 *
 * The usual asio::strand::wrap method can not be used since it returns an
 * asio::detail::wrapped_handler which can not be implicitly cast to a std::function.
 *
 * @tparam Message The type of the message to subscribe to.
 * @tparam Callback The type of the message callback (usually inferred).
 * @param callback The callback to wrap into a strand.
 * @param strand The strand into which to wrap the callback.
 * @return A new std::function object, that dispatches the given callback in the given strand.
 */
template<class Message, class Callback>
inline auto wrap(Callback callback, asio::io_service::strand &strand) {
  if constexpr (std::is_invocable_v<Callback, const Message &>) {
    return [&strand = strand, cb = std::move(callback)](const Message &msg) {
      strand.dispatch([msg = Message(msg), cb = std::move(cb)]() { cb(msg); });
    };
  } else if constexpr (std::is_invocable_v<Callback,
                                           const Message &,
                                           const MessageInfo &>) {
    return [&strand = strand, h = std::move(callback)](const Message &msg,
                                                       const MessageInfo &message_info) {
      strand.dispatch([msg = Message(msg), message_info = MessageInfo(message_info),
                          h = std::move(h)]() { h(msg, message_info); });
    };
  } else if constexpr (std::is_invocable_v<Callback, const std::optional<Message> &>) {
    return [&strand = strand, h = std::move(callback)](const std::optional<Message> &msg) {
      strand.dispatch([msg = std::optional<Message>(msg), h = std::move(h)]() { h(msg); });
    };
  } else if constexpr (std::is_invocable_v<Callback,
                                           const std::optional<Message> &,
                                           const MessageInfo &>) {
    return [&strand = strand, h = std::move(callback)](const std::optional<Message> &msg,
                                                       const MessageInfo &message_info) {
      strand.dispatch([msg = std::optional<Message>(msg),
                          message_info = MessageInfo(message_info),
                          h = std::move(h)]() { h(msg, message_info); });
    };
  } else if constexpr (std::is_invocable_v<Callback>) {
    return [&strand = strand, h = std::move(callback)]() {
      strand.dispatch([h = std::move(h)]() { h(); });
    };
  }
}

template<class Callback>
inline auto wrap(Callback callback, asio::io_service::strand &strand) {
  if constexpr (std::is_invocable_v<Callback>) {
    return [&strand = strand, cb = std::move(callback)]() {
      strand.dispatch([cb = std::move(cb)]() { cb(); });
    };
  }
}

}  // namespace pulicast::transport

#endif //INCLUDE_PULICAST_TRANSPORT_ASIO_TRANSPORT_H_
