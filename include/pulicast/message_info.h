/*     Copyright (C) 2021 Kiteswarms Ltd
 *
 *     This file is part of pulicast.
 *
 *     pulicast is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     pulicast is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with pulicast.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef INCLUDE_PULICAST_MESSAGE_INFO_H_
#define INCLUDE_PULICAST_MESSAGE_INFO_H_

#include "address.h"

namespace pulicast {
struct MessageInfo {
  Address source;
  int lead;

  bool operator==(const MessageInfo &rhs) const {
    return source == rhs.source &&
        lead == rhs.lead;
  }
  bool operator!=(const MessageInfo &rhs) const {
    return !(rhs == *this);
  }
};
}  // namespace pulicast

#endif //INCLUDE_PULICAST_MESSAGE_INFO_H_
