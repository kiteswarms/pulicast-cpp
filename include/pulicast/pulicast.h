/*     Copyright (C) 2021 Kiteswarms Ltd
 *
 *     This file is part of pulicast.
 *
 *     pulicast is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     pulicast is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with pulicast.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef CPP_INCLUDE_PULICAST_PULICAST_H_
#define CPP_INCLUDE_PULICAST_PULICAST_H_

#include <string_view>
#include <string>
#include <memory>
#include <chrono>

#include <asio/io_service.hpp>
#include <asio/ip/udp.hpp>
#include <asio/basic_waitable_timer.hpp>

#include "pulicast/node.h"
#include "pulicast/channel.h"
#include "pulicast/address.h"
#include "pulicast/transport/asio_transport.h"
#include "pulicast/core/transport.h"


using namespace std::chrono_literals;

namespace pulicast {

constexpr uint16_t DEFAULT_PORT = 8765;

class AsioNode : public pulicast::Node {
 public:
  /**
   * @brief Creates a new pulicast node that operates using an asio transport.
   *
   * @param name The name under which the node should announce itself. Duplicate names are allowed.
   * @param port The UDP port under which messages are sent and received.
   * @param ttl The time to live for UDP multicast messages sent by pulicast.
   * @param announcement_period_ns The time between announcements in nanoseconds or anything
   * implicitly convertible.
   * @param io_service The asio::io_service object to be used by the node. If not provided, the node
   * will construct its own io_service for convenience.
   * @param interface_address The address of the network interface on which to join the multicast
   * groups.
   */
  explicit AsioNode(const std::string &name, uint16_t port = DEFAULT_PORT, uint8_t ttl = 0,
           PublishingPeriod announcement_period_ns = 100ms,
           std::shared_ptr<asio::io_service> io_service = std::make_shared<asio::io_service>(),
           std::optional<std::string> interface_address = std::nullopt) :
      pulicast::Node(name, "/",
                     announcement_period_ns,
                     std::make_unique<transport::AsioTransport>(port, ttl, io_service, interface_address)),
      io_service_(io_service),
      announcement_timer_(announcement_period_ns, [this]() {AnnounceNode(); }, *io_service) {
    announcement_timer_.Start();
  }

  /**
   * @brief Creates a new pulicast node that operates using an asio transport.
   *
   * @param name The name under which the node should announce itself. Duplicate names are allowed.
   * @param default_namespace The namespace relative to which channels are to be created.
   * The new node is at the same time a namespace.
   * @param port The UDP port under which messages are sent and received.
   * @param ttl The time to live for UDP multicast messages sent by pulicast.
   * @param announcement_period_ns The time between announcements in nanoseconds or anything
   * implicitly convertible.
   * @param io_service The asio::io_service object to be used by the node. If not provided, the node
   * will construct its own io_service for convenience.
   * @param interface_address The address of the network interface on which to join the multicast
   * groups.
   */
  AsioNode(const std::string &name, const std::string &default_namespace,
           uint16_t port = DEFAULT_PORT, uint8_t ttl = 0,
           PublishingPeriod announcement_period_ns = 100ms,
           std::shared_ptr<asio::io_service> io_service = std::make_shared<asio::io_service>(),
           std::optional<std::string> interface_address = std::nullopt) :
      pulicast::Node(name, default_namespace,
                     announcement_period_ns,
                     std::make_unique<transport::AsioTransport>(port, ttl, io_service, interface_address)),
      io_service_(io_service),
      announcement_timer_(announcement_period_ns, [this]() {AnnounceNode(); }, *io_service) {
    announcement_timer_.Start();
  }

  /**
   * @return A shared pointer to the asio io_service object that is used by this node.
   */
  std::shared_ptr<asio::io_service> GetService() {
    return io_service_;
  }

 private:
  // Note: It is important, that the io_service_ is the topmost member in this class to ensure that
  //   it gets deleted last. Otherwise we might get segfaults or memory leaks. This is due to the
  //   non-advised storage of the io_service inside a shared ptr.
  //   (see https://github.com/chriskohlhoff/asio/issues/16)
  //   also see issue 84
  std::shared_ptr<asio::io_service> io_service_;
  transport::PeriodicTimer<> announcement_timer_;
};

}  // namespace pulicast


#endif //CPP_INCLUDE_PULICAST_PULICAST_H_
