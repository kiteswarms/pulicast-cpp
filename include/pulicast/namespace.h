/*     Copyright (C) 2021 Kiteswarms Ltd
 *
 *     This file is part of pulicast.
 *
 *     pulicast is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     pulicast is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with pulicast.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef INCLUDE_PULICAST_NAMESPACE_H_
#define INCLUDE_PULICAST_NAMESPACE_H_

#include <string>

#include "pulicast/channel.h"
#include "pulicast/core/channel_store.h"

namespace pulicast {

  /**
   * @brief A pulicast namespace provides access to channels in a channel store relative to a
   * given path.
   */
  class Namespace {

   public:
    /**
     * @brief Crates a new Namespace.
     * @param channel_store The channel store from which to retrieve the channels.
     * @param path The path relative to which the channels should be referred.
     */
    explicit Namespace(core::ChannelStore& channel_store, const std::string& path = "/") :
      path_(path), channel_store_(channel_store) {
      // WARNING: The node class assumes, that the channel_store reference passed here will not be
      // touched and therefore an uninitialized reference can be passed.
    }

    virtual Channel &operator[](const std::string &name) const {
      return channel_store_.GetChannel(pulicast::core::ResolveName(name, path_));
    }

    Namespace operator/(const std::string& child) const {
      return Namespace(channel_store_, pulicast::core::ResolveName(child, path_));
    }

    Namespace& operator=(const std::string& new_name) {
      path_ = new_name;
      return *this;
    }

    explicit operator std::string () const {
      return path_;
    }

    explicit operator Channel& () const {
      return channel_store_.GetChannel(path_);
    }

   private:
    std::string path_;
    core::ChannelStore& channel_store_;
  };
}  // namespace pulicast

#endif //INCLUDE_PULICAST_NAMESPACE_H_
