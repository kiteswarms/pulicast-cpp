/*     Copyright (C) 2021 Kiteswarms Ltd
 *
 *     This file is part of pulicast.
 *
 *     pulicast is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     pulicast is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with pulicast.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef CPP_INCLUDE_PULICAST_CORE_UTILS_NETWORK_BYTE_ORDERING_H_
#define CPP_INCLUDE_PULICAST_CORE_UTILS_NETWORK_BYTE_ORDERING_H_

// This is just a trick to ensure that embedded can inject their own header defining htonx/ntohx
#ifdef CUSTOM_HTON
 #include CUSTOM_HTON
#else
 #include <netinet/in.h>
#endif

#endif //CPP_INCLUDE_PULICAST_CORE_UTILS_NETWORK_BYTE_ORDERING_H_
