/*     Copyright (C) 2021 Kiteswarms Ltd
 *
 *     This file is part of pulicast.
 *
 *     pulicast is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     pulicast is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with pulicast.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef INCLUDE_PULICAST_CORE_UTILS_SCATTER_GATHER_BUFFER_H_
#define INCLUDE_PULICAST_CORE_UTILS_SCATTER_GATHER_BUFFER_H_

#include <asio/buffer.hpp>
#include "pulicast/core/utils/stack_stack.h"

namespace pulicast::core {
  using ScatterGatherBuffer = StackStack<asio::const_buffer>;
}

#endif //INCLUDE_PULICAST_CORE_UTILS_SCATTER_GATHER_BUFFER_H_
