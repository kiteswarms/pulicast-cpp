/*     Copyright (C) 2021 Kiteswarms Ltd
 *
 *     This file is part of pulicast.
 *
 *     pulicast is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     pulicast is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with pulicast.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef INCLUDE_PULICAST_CORE_UTILS_NAME_RESOLUTION_H_
#define INCLUDE_PULICAST_CORE_UTILS_NAME_RESOLUTION_H_

#include <string>
#include <filesystem>
namespace fs = std::filesystem;

namespace pulicast::core {



/**
 * @brief Resolves the name of a pulicast channel or node within the given namespace.
 *
 * See the documentation on name resolution for details on how names are resolved exactly.
 *
 * @param name The name to resolve.
 * @param name_space The namespace we are in. "/" is the root.
 * @return The resolved name.
 */
inline std::string ResolveName(const std::string &name, const std::string& name_space) {
  // Resolve name
  auto s = (fs::path("/") / name_space / name).lexically_normal().relative_path().string();

  // Remove leading and trailing slashes
  auto is_not_slash = [](std::string::value_type ch) {return ch != '/';};
  s.erase(s.begin(), std::find_if(s.begin(), s.end(), is_not_slash));
  s.erase(std::find_if(s.rbegin(), s.rend(), is_not_slash).base(), s.end());
  return s;
}

} // namespace pulicast::core

#endif //INCLUDE_PULICAST_CORE_UTILS_NAME_RESOLUTION_H_
