/*     Copyright (C) 2021 Kiteswarms Ltd
 *
 *     This file is part of pulicast.
 *
 *     pulicast is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     pulicast is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with pulicast.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef INCLUDE_PULICAST_CORE_UTILS_EXTRACT_FROM_BUFFER_H_
#define INCLUDE_PULICAST_CORE_UTILS_EXTRACT_FROM_BUFFER_H_

#include <optional>
#include <asio/buffer.hpp>

#include "pulicast/core/utils/network_byte_ordering.h"

namespace pulicast::core {

template<class T>
std::optional<T> Extract(asio::mutable_buffer &buffer);

template<>
inline std::optional<uint8_t> Extract<uint8_t>(asio::mutable_buffer &buffer) {
  if (asio::buffer_size(buffer) >= sizeof(uint8_t)) {
    auto ret = *asio::buffer_cast<uint8_t *>(buffer);
    buffer = buffer + sizeof(uint8_t);
    return ret;
  }
  return std::nullopt;
}

template<>
inline std::optional<uint32_t> Extract<uint32_t>(asio::mutable_buffer &buffer) {
  if (asio::buffer_size(buffer) >= sizeof(uint32_t)) {
    auto ret = ntohl(*asio::buffer_cast<uint32_t *>(buffer));
    buffer = buffer + sizeof(uint32_t);
    return ret;
  }
  return std::nullopt;
}

template<>
inline std::optional<std::string_view> Extract<std::string_view>(asio::mutable_buffer &buffer) {
  auto ret = std::string_view(asio::buffer_cast<char *>(buffer));
  if (asio::buffer_size(buffer) >= ret.size() + 1) {
    buffer = buffer + ret.size() + 1;
    return ret;
  }
  return std::nullopt;
}

inline std::optional<std::string_view> ExtractStringView(asio::mutable_buffer &buffer,
                                                         size_t size) {
  if (asio::buffer_size(buffer) >= size) {
    auto ret = std::string_view(asio::buffer_cast<char *>(buffer), size);
    buffer = buffer + ret.size();
    return ret;
  }
  return std::nullopt;
}

}  // namespace pulicast::core

#endif //INCLUDE_PULICAST_CORE_UTILS_EXTRACT_FROM_BUFFER_H_
