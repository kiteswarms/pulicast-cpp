/*     Copyright (C) 2021 Kiteswarms Ltd
 *
 *     This file is part of pulicast.
 *
 *     pulicast is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     pulicast is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with pulicast.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef CPP_INCLUDE_PULICAST_CORE_DUPLEX_CHANNEL_MAP_H_
#define CPP_INCLUDE_PULICAST_CORE_DUPLEX_CHANNEL_MAP_H_

#include <utility>
#include <memory>
#include <string>
#include <unordered_map>
#include <pulicast/core/utils/name_resolution.h>

#include "pulicast/channel.h"

namespace pulicast::core {

/**
 * @brief A container that holds Channel objects and lazily creates them on demand.
 */
class ChannelStore {
 public:
  /**
   * @brief Is called whenever a new channel object has been added to the map.
   */
  std::function<void(const std::string&)> on_channel_added_callback_;

  ChannelStore(std::unique_ptr<Transport> transport)
      : transport_(std::move(transport)) {}
  ChannelStore(const ChannelStore&) = delete;
  ChannelStore& operator=(const ChannelStore&) = delete;
  ChannelStore(ChannelStore&&) = delete;
  ChannelStore& operator=(ChannelStore&&) = delete;
  virtual ~ChannelStore() = default;

  /**
   * @brief Returns a reference to the Channel object for the given channel name.
   *
   * If a channel object does not already exist, it will be created on demand.
   *
   * @param name The name of the channel.
   * @return A reference to the Channel object for the channel name.
   */
  Channel& GetChannel(const std::string &name) {
    auto channel_it = items_.find(name);
    if (channel_it == items_.end()) {
      channel_it = items_.try_emplace(name, name, *transport_).first;
      if(on_channel_added_callback_)
        on_channel_added_callback_(name);
    }
    return channel_it->second;
  }

  auto begin() {
    return items_.begin();
  }

  auto end() {
    return items_.end();
  }

  auto find(const std::string& name) {
    return items_.find(name);
  }

 private:
  // Note: The transport (partly) owns the asio io_service object that is used (but not owned!)
  //   by the channels. Therefore the items_ must be below the transport_ here. Otherwise we might
  //   cause memory corruption upon deleting the pulicast node.
  //   See Issue 84 for details.
  std::unique_ptr<Transport> transport_;
  std::unordered_map<std::string, Channel> items_;
};

}  // namespace pulicast::core

#endif //CPP_INCLUDE_PULICAST_CORE_DUPLEX_CHANNEL_MAP_H_
