/*     Copyright (C) 2021 Kiteswarms Ltd
 *
 *     This file is part of pulicast.
 *
 *     pulicast is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     pulicast is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with pulicast.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef INCLUDE_PULICAST_CORE_LEAD_EXTRACTOR_H_
#define INCLUDE_PULICAST_CORE_LEAD_EXTRACTOR_H_

#include<optional>
#include<unordered_map>

#include <asio/buffer.hpp>

#include "pulicast/address.h"
#include "pulicast/core/utils/extract_from_buffer.h"

namespace pulicast::core {

class LeadExtractor {
 public:
  std::optional<int> ExtractLead(asio::mutable_buffer& b, Address source) {
    auto sequence_number = core::Extract<uint32_t>(b);
    if (sequence_number.has_value()) {
      return ComputeLeadAndUpdateLastSequenceNumber(source, *sequence_number);
    }
    return std::nullopt;
  }

  // TODO(max): this function smells funny and can probably be improved
  int ComputeLeadAndUpdateLastSequenceNumber(const Address &source, uint32_t seq_no) {
    auto[it, was_newly_inserted] = last_seq_numbers_.try_emplace(source, seq_no);
    if (was_newly_inserted) {
      return 1;
    }
    auto lead = static_cast<int>(seq_no - it->second);
    it->second = std::max(it->second, seq_no);
    return lead;
  }

 private:
  std::unordered_map<Address, uint32_t, AddressHash, AddressEqual> last_seq_numbers_;

};

}  // namespace pulicast::core

#endif //INCLUDE_PULICAST_CORE_LEAD_EXTRACTOR_H_
