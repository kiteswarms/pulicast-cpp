/*     Copyright (C) 2021 Kiteswarms Ltd
 *
 *     This file is part of pulicast.
 *
 *     pulicast is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     pulicast is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with pulicast.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef INCLUDE_PULICAST_ADDRESS_H_
#define INCLUDE_PULICAST_ADDRESS_H_

namespace pulicast {

struct Address {
  bool operator==(const Address &rhs) const {
    return ip == rhs.ip &&
        port == rhs.port;
  }
  bool operator!=(const Address &rhs) const {
    return !(rhs == *this);
  }
  uint32_t ip;
  uint16_t port;
};

struct AddressHash {
  std::size_t operator()(const Address& s) const
  {
    // TODO(max): is this a good hashing algorithm?
    size_t seed = 0;
    seed ^= std::hash<uint32_t>()(s.ip) + 0x9e3779b9 + (seed << 6u) + (seed >> 2u);
    seed ^= std::hash<uint16_t>()(s.port) + 0x9e3779b9 + (seed << 6u) + (seed >> 2u);
    return seed;
  }
};

struct AddressEqual {
  bool operator()(const Address& lhs, const Address& rhs) const
  {
    return lhs == rhs;
  }
};

}  // namespace pulicast

// TODO(max): maybe we can use google abseils fixed arrays to handle buffers?
//  https://github.com/abseil/abseil-cpp/blob/master/absl/container/fixed_array.h
// or spans? or boost buffers?

#endif // INCLUDE_PULICAST_ADDRESS_H_