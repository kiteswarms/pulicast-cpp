/*     Copyright (C) 2021 Kiteswarms Ltd
 *
 *     This file is part of pulicast.
 *
 *     pulicast is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     pulicast is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with pulicast.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef INCLUDE_PULICAST_CHANNEL_H_
#define INCLUDE_PULICAST_CHANNEL_H_

#include <unordered_map>
#include <utility>
#include <vector>
#include <utility>
#include <string_view>
#include <functional>
#include <memory>
#include <chrono>
#include <limits>
#include <string>

#include <asio/buffer.hpp>
#include <pulicast/core/utils/extract_from_buffer.h>

#include "pulicast/serialization.h"
#include "pulicast/address.h"
#include "pulicast/core/subscription_dispatcher.h"
#include "pulicast/core/transport.h"
#include "pulicast/core/sequence_numbered_sender.h"
#include "pulicast/core/lead_extractor.h"

namespace pulicast {
using PublishingPeriod = std::chrono::nanoseconds;
const PublishingPeriod kUnknownPublishingPeriod = std::chrono::nanoseconds::zero();

/**
 * @brief A handle to a pulicast channel which allows for publishing and subscribing to the channel.
 *
 * The Channel class allows to publish and subscribe to messages on pulicast channels.
 * Messages are everything that can be serialized by pulicast.
 *
 * A pulicast channel is identified by its name. There can be any number of subscribers
 * and publishers on a single channel as well as any number of different message types
 * delivered via the channel. However the typical usage pattern foresees a single publisher and a
 * single message type.

 * @note Channel objects are typically not instantiated directly but rather obtained via a Node
 * object to prevent duplicate channel objects for a single pulicast channel and to ensure proper
 * functioning of the discovery mechanisms of pulicast.
 */
class Channel {
 public:

  /**
   * @brief The period in ns with which you intend to publish on this channel.
   * Leave at zero if unspecified.
   *
   * Specifying this value helps receivers identify dodgy behavior by comparing the actual vs. the
   * announced publishing period.
   */
  PublishingPeriod publishing_period_;

  /**
   * @brief Constructs a new channel object. Do not call this directly. Use a pulicast node to
   * acquire a channel.
   *
   * @param name The name of the channel to publish/subscribe on.
   * @param transport The transport to be used by the channel.
   */
  Channel(const std::string &name, core::Transport &transport) :
      publishing_period_(kUnknownPublishingPeriod),
      name_(name),
      transport_(transport),
      sequence_numbered_sender_(transport.MakeChannelSender(name)) {}

  Channel() = delete;

  /**
   * @note Even though this class is implicitly not copyable due to having a unique_ptr member,
   * we delete its copy constructor explicitly because channels should not be copied ever even if
   * some future implementation changes the use of the unique_ptr.
   */
  Channel(const Channel &) = delete;

  Channel &operator=(const Channel &) = delete;

  Channel(Channel &&other) = delete;

  Channel &operator=(Channel &&) = delete;

  ~Channel() = default;

  /**
   * @brief Publish raw bytes on this channel.
   *
   * @param data Pointer to the data to publish.
   * @param size Amount of data to publish.
   */
  void Publish(const char *data, size_t size) {
    Publish(core::ScatterGatherBuffer{asio::buffer(data, size)});
  }

  /**
   * @brief Publish a scatter-gather buffer on this channel.
   *
   * @param scatter_gather_buffer A stack-based scatter-gather buffer to publish.
   */
  void Publish(const core::ScatterGatherBuffer& scatter_gather_buffer) {
    sequence_numbered_sender_.Send(scatter_gather_buffer);
  }

  /**
 * @brief Subscribes to raw bytes arriving on this channel.
 *
 * @param callback The callback to call when something arrives.
 */
  void Subscribe(std::function<void(const char *, size_t, const MessageInfo &)> callback) {
    GetDispatcher().Subscribe(std::move(callback));
  }

  /**
   * @brief Subscribes to raw bytes arriving on this channel.
   *
   * @param callback The callback to call when something arrives.
   * @param min_lead The min required message lead.
   */
  void Subscribe(std::function<void(const char *, size_t, const MessageInfo &)> callback,
                 int min_lead) {
    Subscribe([callback = std::move(callback), min_lead]
                  (const char *data, size_t size, const MessageInfo &message_info) {
      if (message_info.lead >= min_lead) {
        callback(data, size, message_info);
      }
    });
  }

  /**
   * @brief Subscribe to intact messages and their message info on this channel.
   *
   * @tparam Message The type of messages to subscribe to.
   * @param callback The callback to call when a new message arrived.
   * @param min_lead The min required message lead.
   */
  template<class Message>
  void Subscribe(std::function<void(const Message &, const MessageInfo &)> callback,
                 int min_lead = std::numeric_limits<int>::min()) {
    Subscribe([callback = std::move(callback)]
                  (const char *data, size_t size, const MessageInfo &message_info) {
      auto message = Decode<Message>({data, size});
      if (message.has_value()) {
        callback(*message, message_info);
      }
    }, min_lead);
  }

  /**
   * @brief Subscribe to (also possibly broken) messages and their message info on this channel.
   *
   * This variant of Subscribe() delivers the message inside an std::optional. When the message
   * could not be decoded, the callback is still being called with an empty message parameter.
   *
   * @tparam Message The type of messages to subscribe to.
   * @param callback The callback to call when a new message arrived.
   * @param min_lead The min required message lead.
   */
  template<class Message>
  void Subscribe(std::function<void(const std::optional<Message> &, const MessageInfo &)> callback,
                 int min_lead = std::numeric_limits<int>::min()) {
    Subscribe([callback = std::move(callback)]
                  (const char *data, size_t size, const MessageInfo &message_info) {
      callback(Decode<Message>({data, size}), message_info);
    }, min_lead);
  }

  /**
   * @brief Subscribe to intact messages on this channel.
   *
   * @tparam Message The type of messages to subscribe to.
   * @param callback The callback to call when a new message arrived.
   * @param min_lead The min required message lead.
   */
  template<class Message>
  void Subscribe(std::function<void(const Message &)> callback,
                 int min_lead = std::numeric_limits<int>::min()) {
    Subscribe([callback = std::move(callback)]
                  (const char *data, size_t size, const MessageInfo &) {
      auto message = Decode<Message>({data, size});
      if (message.has_value()) {
        callback(*message);
      }
    }, min_lead);
  }

  /**
   * @brief Subscribe to (also possibly broken) messages on this channel.
   *
   * This variant of Subscribe() delivers the message inside an std::optional. When the message
   * could not be decoded, the callback is still being called with an empty message parameter.
   *
   * @tparam Message The type of messages to subscribe to.
   * @param callback The callback to call when a new message arrived.
   * @param min_lead The min required message lead.
   */
  template<class Message>
  void Subscribe(std::function<void(const std::optional<Message> &)> callback,
                 int min_lead = std::numeric_limits<int>::min()) {
    Subscribe([callback = std::move(callback)]
                  (const char *data, size_t size, const MessageInfo & /*message_info*/) {
      callback(Decode<Message>({data, size}));
    }, min_lead);
  }

  /**
   * @brief Subscribe to the event that any message arrived on this channel.
   *
   * In this variant of Subscribe() the callback takes no arguments.
   * This is for the rare case where you just care for the fact that a message arrived but not what
   * message.
   *
   * @param callback The callback to call when any message arrived on the channel.
   * @param min_lead The min required message lead.
   */
  void Subscribe(std::function<void()> callback,
                 int min_lead = std::numeric_limits<int>::min()) {
    Subscribe([callback = std::move(callback)](const char *data, size_t  /*size*/, const MessageInfo & /*message_info*/) {
      callback();
    }, min_lead);
  }

  /**
   * @brief Same as the corresponding Subscribe() but avoids duplicate or outdated message delivery.
   *
   * Outdated and duplicate messages are avoided by using a minimum lead of 0.
   *
   * @param callback The callback to call when a new message arrives.
   */
  void SubscribeOrdered(std::function<void(const char *, size_t, const MessageInfo &)> callback) {
    Subscribe(std::move(callback), 0);
  }

  /**
   * @brief Same as the corresponding Subscribe() but avoids duplicate or outdated message delivery.
   *
   * Outdated and duplicate messages are avoided by using a minimum lead of 0.
   *
   * @param callback The callback to call when a new message arrives.
   */
  template<class Message>
  void SubscribeOrdered(std::function<void(const Message &, const MessageInfo &)> callback) {
    Subscribe(callback, 0);
  }

  /**
   * @brief Same as the corresponding Subscribe() but avoids duplicate or outdated message delivery.
   *
   * Outdated and duplicate messages are avoided by using a minimum lead of 0.
   *
   * @param callback The callback to call when a new message arrives.
   */
  template<class Message>
  void SubscribeOrdered(std::function<void(const std::optional<Message> &,
                                           const MessageInfo &)> callback) {
    Subscribe<Message>(callback, 0);
  }

  /**
   * @brief Same as the corresponding Subscribe() but avoids duplicate or outdated message delivery.
   *
   * Outdated and duplicate messages are avoided by using a minimum lead of 0.
   *
   * @param callback The callback to call when a new message arrives.
   */
  template<class Message>
  void SubscribeOrdered(std::function<void(const Message &)> callback) {
    Subscribe<Message>(callback, 0);
  }

  /**
   * @brief Same as the corresponding Subscribe() but avoids duplicate or outdated message delivery.
   *
   * Outdated and duplicate messages are avoided by using a minimum lead of 0.
   *
   * @param callback The callback to call when a new message arrives.
   */
  template<class Message>
  void SubscribeOrdered(std::function<void(const std::optional<Message> &)> callback) {
    Subscribe<Message>(callback, 0);
  }

  /**
   * @brief Same as the corresponding Subscribe() but avoids duplicate or outdated message delivery.
   *
   * Outdated and duplicate messages are avoided by using a minimum lead of 0.
   *
   * @param callback The callback to call when a new message arrives.
   */
  void SubscribeOrdered(std::function<void()> callback) {
    Subscribe(std::move(callback), 0);
  }

  /**
   * @brief Check if this node subscribes to the channel.
   *
   * To determine if other nodes subscribe to this channel use the discovery mechanism.
   */
  bool IsSubscribed() const {
    return static_cast<bool>(subscription_dispatcher_);
  }

  /**
   * @brief Check if this node publishes to the channel.
   *
   * To determine if other nodes publish to this channel use the discovery mechanism.
   */
  bool IsPublished() const {
    return sequence_numbered_sender_.GetSequenceNumber() > 0;
  }

  /**
   * @return The full name including namespace of the channel.
   */
  std::string_view GetName() const {
    return name_;
  }

 private:
  std::string name_;
  core::Transport &transport_;
  std::unique_ptr<core::SubscriptionDispatcher> subscription_dispatcher_;
  core::SequenceNumberedSender sequence_numbered_sender_;
  core::LeadExtractor lead_extractor_;

  core::SubscriptionDispatcher &GetDispatcher() {
    if (!subscription_dispatcher_) {
      subscription_dispatcher_ = std::make_unique<core::SubscriptionDispatcher>();
      transport_.StartReceivingMessagesFor(
          name_,
          [this](asio::mutable_buffer &buffer, Address address) { OnNewPacket(buffer, address); });
    }
    return *subscription_dispatcher_;
  }

  void OnNewPacket(asio::mutable_buffer &buffer, Address address) {
    auto lead = lead_extractor_.ExtractLead(buffer, address);
    if (lead.has_value() && subscription_dispatcher_) {
      subscription_dispatcher_->Dispatch(buffer, address, *lead);
    }
  }

};

template<class T>
inline pulicast::Channel &operator<<(pulicast::Channel &channel, const T &message) {
  EncodeAndThenCall(message, [&](const auto& buffer) { channel.Publish(buffer); });
  return channel;
}

}  // namespace pulicast

#endif //INCLUDE_PULICAST_CHANNEL_H_
