/*     Copyright (C) 2021 Kiteswarms Ltd
 *
 *     This file is part of pulicast.
 *
 *     pulicast is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     pulicast is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with pulicast.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <string>

#include <pulicast/pulicast.h>
#include <pulicast/distributed_setting.h>
#include <boost/date_time/posix_time/posix_time.hpp>

using namespace std::string_literals;

double compute_function(std::string_view function, double amplitude, double offset, double freq, double x) {
  if (function == "sin") {
    return sin(x * freq + offset) * amplitude;
  }
  if (function == "cos") {
    return cos(x * freq + offset) * amplitude;
  }
  if (function == "saw") {
    return ((x*freq + offset) - floor(x*freq + offset)) * amplitude;
  }
  return 0;
}

int main() {

  using namespace pulicast::experimental::distributed_settings;
  auto node = pulicast::AsioNode("function_generator");

  auto function_name = DistributedSetting("sin"s, "function_name", node, [](){}, AcceptOnly({"sin"s, "cos"s, "saw"s}));
  auto sampling_period = DistributedSetting(0.1, "sampling_period", node, [](){}, AcceptRange(0.01, 1.0));
  auto amplitude = DistributedSetting(1.0, "amplitude", node, [](){}, AcceptAtLeast<double>(0));
  auto offset = DistributedSetting(0.0, "offset", node);
  auto freq = DistributedSetting(1.0, "freq", node, [](){}, AcceptAtLeast(0.0));

  auto timer = asio::basic_waitable_timer<std::chrono::system_clock>(*node.GetService(), std::chrono::system_clock::duration::zero());

  std::function<void(const asio::error_code)> publish_function = [&](const asio::error_code &) {
    double x = std::chrono::duration_cast<std::chrono::duration<double> >(timer.expires_at().time_since_epoch()).count();
    auto y = compute_function(*function_name, *amplitude, *offset, *freq, x);
//    std::cout << x << " " << y << std::endl;
    node["generated_signal"] << y;

    auto sampling_period_chrono = std::chrono::duration_cast<std::chrono::system_clock::duration>(
        std::chrono::duration<double>(*sampling_period));
    timer.expires_at(timer.expires_at() + sampling_period_chrono);
    timer.async_wait(publish_function);
  };

  timer.async_wait(publish_function);

  std::cout << "My ID is " << node.GetSessionID() << std::endl;

  node.GetService()->run();
}

/** @example function_generator.cpp
 *  This example demonstrates how a simple function generator can be configured using puliconf and
 *  distributed settings. Note how the distributed settings act like read-only `std::optional`s.
 */
