/*     Copyright (C) 2021 Kiteswarms Ltd
 *
 *     This file is part of pulicast.
 *
 *     pulicast is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     pulicast is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with pulicast.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <pulicast/pulicast.h>

#include <iostream>
#include <string>


int main() {
  pulicast::AsioNode channels("stdin_publisher");
  auto& channel = channels["example_channel"];

  std::string message;
  while ( std::getline(std::cin, message) ) {
    channel << message;
  }

  // Note: for the sake of simplicity the invocation of the asio eventloop has been omitted here.
}

/** @example publish_stdin.cpp
 * This code reads lines from stdin and publishes them as `std::string` messages on the
 * `example_channel`
 *
 */