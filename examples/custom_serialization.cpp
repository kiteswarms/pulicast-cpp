/*     Copyright (C) 2021 Kiteswarms Ltd
 *
 *     This file is part of pulicast.
 *
 *     pulicast is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     pulicast is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with pulicast.  If not, see <https://www.gnu.org/licenses/>.
 */

#include<tuple>
#include<vector>
#include<optional>
#include<utility>
#include <asio/buffer.hpp>

#include <pulicast/serialization.h>

struct CustomType {
  bool just_a_flag;
};

template<class Target>
inline void EncodeAndThenCall(const CustomType &message, Target &&target) {
  pulicast::EncodeAndThenCall(message.just_a_flag, std::move(target));
}

inline void DecodeMessageInto(const asio::const_buffer &buffer, std::optional<CustomType> &target) {
  auto flag = pulicast::Decode<bool>(buffer);

  if(flag.has_value()) {
    target = CustomType{flag.value()};
  } else {
    target = std::nullopt;
  }
}

#include <pulicast/pulicast.h>

int main() {
  auto node = pulicast::AsioNode("test");

  auto &channel = node["test"];

  channel.Subscribe<CustomType>([](const CustomType &msg) {});

  channel << CustomType{false};
}

/** @example custom_serialization.cpp
 * This example demonstrates how to implement custom serialization code for a custom type, that just
 * wraps a bool.
 */