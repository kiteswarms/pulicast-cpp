/*     Copyright (C) 2021 Kiteswarms Ltd
 *
 *     This file is part of pulicast.
 *
 *     pulicast is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     pulicast is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with pulicast.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <iostream>
#include <string>

#include <pulicast/pulicast.h>
#include <asio/streambuf.hpp>
#include <asio/read_until.hpp>
#include <asio/posix/stream_descriptor.hpp>

class ChatClient {
 public:
  ChatClient(std::shared_ptr<asio::io_service> io_service, pulicast::Channel& channel) :
      channel_(channel),
      console_in_stream_(*io_service, ::dup(STDIN_FILENO))
  {
    // Subscribe incoming messages via lambda expression
    channel_.Subscribe<std::string>(
        [](const std::string &msg, const pulicast::MessageInfo& message_info) {
          std::cout << message_info.source.port << ": " << msg << std::endl;
        });

    SetupConsoleHandler();
  }

  void HandleConsoleInput(const asio::error_code& error, std::size_t length) {
    // Get string from console_in_buffer_
    char message[length];
    console_in_buffer_.sgetn(message, length);
    message[length-1] = '\0';  // replace the newline by null termination

    // Publish ZCOM logmessage
    channel_ << std::string(message);
  }

  void SetupConsoleHandler() {
    asio::async_read_until(console_in_stream_, console_in_buffer_, '\n',
    [this](const asio::error_code& ec, std::size_t bytes) {
      HandleConsoleInput(ec, bytes);
      SetupConsoleHandler();
    });
  }


 private:
  pulicast::Channel& channel_;

  asio::streambuf console_in_buffer_;
  asio::posix::stream_descriptor console_in_stream_;
};

int main() {
  const auto kChannelName = "channel_1";

  pulicast::AsioNode node("chatter");
  auto service = node.GetService();
  ChatClient client(service, node[kChannelName]);
  service->run();
}

/** @example asio_chat_client.cpp
 * A chat client implemented using the asio event loop.
 * Unfortunately it looks a bit involved since reading from stdin using ASIO is a pita.
 */
