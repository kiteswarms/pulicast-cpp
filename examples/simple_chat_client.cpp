/*     Copyright (C) 2021 Kiteswarms Ltd
 *
 *     This file is part of pulicast.
 *
 *     pulicast is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     pulicast is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with pulicast.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <iostream>
#include <string>
#include <thread>

#include <pulicast/pulicast.h>

int main() {
  const std::string kChannelName = "channel_1";

  pulicast::AsioNode node("simple_chat_client");
  auto& channel = node[kChannelName];

  node[kChannelName].Subscribe<std::string>(
      [](const std::string &msg, pulicast::MessageInfo message_info) {
        std::cout << message_info.source.port << ": " << msg << std::endl;
      });

  auto recv_thread = std::thread([&node]() {node.GetService()->run();});

  std::string msg;
  while ( std::getline(std::cin, msg) ) {
    channel << msg;
  }
}

/** @example simple_chat_client.cpp
 * This is a very primitive implementation of a chat client.
 * It handles incoming messages in a separate thread to allow for reading stdin in the main thread.
 * Note that this is a highly discouraged design pattern used here just for brevity.
 * A single-threaded implementation can be found in `asio_chat_client.cpp`.
 */