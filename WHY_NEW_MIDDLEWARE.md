# Why build another middleware?

Is there no existing solution on the market, that fits our needs? What makes Pulicast better than the competition?
Why do we use UDP multicast and not any other protocol?

# Other Protocols than UDP
See also [here](https://en.wikipedia.org/wiki/Transport_layer#Comparison_of_transport_layer_protocols) for an overview.

## RDP -- Reliable Data Protocol
A protocol next to UDP or TCP.
RDP does not guarantee sequenced delivery (which can cause problems, see post below). Also according to [Wikipedia](https://en.wikipedia.org/wiki/Reliable_Data_Protocol) it is "connection oriented" (sounds like not suitable for multicast) and:   
> The Reliable Data Protocol has not gained popularity, though experimental implementations for BSD exist.

In [this Paper from 1987](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.71.8717&rep=rep1&type=pdf), the author concludes that:
> The TCP implementation always did better than the RDP implementation when application data sizes were small (less than 64 bytes). Apparently TCP’s ability to aggregate data into larger packets does pay off when applications send data in small chunks

## RUDP -- Reliable User Datagram Protocol
Extends UDP by means of adding the following features:

* Acknowledgment of received packets
* Windowing and flow control
* Retransmission of lost packets
* Over buffering (Faster than real-time streaming)
    
I found three implementations (including the one suggested by @elias which seems to be the most mature one) [1](https://github.com/Tetram100/RUDP), [2](https://github.com/evercomer/rudp) [1](http://enet.bespin.org/).
Reading into the ENet library documentation if found, that it features package sequencing via a package counter, channels and optional reliability. It is unclear if it runs on an embedded system. It uses a polled event model. It does not seem to support multicast.

## TCP
Is stream oriented and not message oriented. Seems to work rather well for smaller packages because messages are bundled in larger packages. 
But what happens with latency then? Read [here](https://stackoverflow.com/questions/855544/is-there-a-way-to-flush-a-posix-socket)
Is not suitable for multicast.

## [UDT](http://udt.sourceforge.net/)
Seems to be made for larger chunks of data.

## [SCTP](https://en.wikipedia.org/wiki/Stream_Control_Transmission_Protocol)
 Might be interesting (is standartized, reliable ordered message stream, widely adopted, multi-streaming). 
 However it is unclear if an embedded implementation exists/is feasible. It also does not seem to support multicast.
 
## [OpenGameProtocol](http://www.open-game-protocol.org/)
Too specific to games.

# Other Middlewares
What we actually want here is a DDS (data distribution service). 
This is standardized by the [OMG](https://www.omg.org/) (object management group) [here](https://www.omg.org/omg-dds-portal/).

## eProsima
Is a company that builds such open source middlewares.
Their [fast RTPS](https://www.eprosima.com/index.php/products-all/eprosima-fast-rtps) (Real Time Publish Subscribe Protocol) is the [default used by ROS2](https://index.ros.org/doc/ros2/Concepts/DDS-and-ROS-middleware-implementations/).
It does not run on embedded platforms. 
They however offer [XRCE](https://www.eprosima.com/index.php/products-all/eprosima-micro-xrce-dds) for embedded platforms.
XRCE relies on a central server to which the (embeded) XRCE clients connect. The server acts as a gateway to other DDS.
This means that if two embedded nodes need to communicate, they rely on the gateway. 
Read more [here](https://objectcomputing.com/resources/publications/sett/october-2019-dds-for-extremely-resource-constrained-environments)

## OpenDDS

[OpenDDS](https://opendds.org/) is an open source implementation of the DDS standard developed by [Object Computing](https://objectcomputing.com/).
It is based on [CORBA](https://corba.org).

## [CORBA](https://corba.org)
Is more about syncing objects over a network and operating on remote objects.

## (mirco)ROS2
[uROS](https://micro-ros.github.io/) is based on the XRCE middleware. Requires using the [NuttX](https://nuttx.apache.org/) embedded RTOS.
Relies on the gateway just like XRCE. Bosch is involved as well as eProsima

## [Aeron](https://github.com/real-logic/aeron)
> Efficient reliable UDP unicast, UDP multicast, and IPC message transport

No Python support. Since it is reliable, it needs to keep a message buffer at the publisher side and NAKs for resending.
This is too expensive for embedded probably.

## [Mutalk](https://github.com/reddec/mutalk)

C only but same idea of mapping channel names to multicast groups!

## Future Research
Maybe it is worth to investigate those further:

* There is a similiarity between our property system and [distributed hash tables](https://en.wikipedia.org/wiki/Distributed_hash_table)
* [Pragmatic General Multicast](https://en.wikipedia.org/wiki/Pragmatic_General_Multicast)
* [Scalable Reliable Multicast](https://en.wikipedia.org/wiki/Scalable_Reliable_Multicast)
* [Reliable Multicast Library by HummingbordCode](http://www.hummingbirdcode.net/)
* [Spread Toolkit](http://www.spread.org/)
* [The Actor Model](https://en.wikipedia.org/wiki/Actor_model)
* [The MAVLink Protocol](https://mavlink.io/en/about/overview.html)
* [ansible-multicast-graph](https://github.com/optiz0r/ansible-multicast-graph)
* [Simple Binary Encoding](https://real-logic.github.io/simple-binary-encoding/)
* [MIRA](https://en.wikipedia.org/wiki/Middleware_for_Robotic_Applications)