# Pulicast for C++ -- API Documentation
Pulicast is a Publish/Subscribe middleware on the basis of UDP multicast groups.

This page contains just the API documentation. 
**For full documentation refer to the [pulicast python documentation](https://kiteswarms.gitlab.io/pulicast-python/).**
The C++ and Python API are nearly identical, therefore the C++ API documentation and examples
should suffice to develop with pulicast in C++.

## Installation and Usage
`.deb` packages can be found on the 
[releases page](https://gitlab.com/kiteswarms/pulicast-cpp/-/releases) and in the 
[package registry](https://gitlab.com/kiteswarms/pulicast-cpp/-/packages).
It is a header-only library whose files will be placed in `/usr/include/pulicast/`.
To use it in a CMake project use:

```cmake
find_package(pulicast)
target_link_libraries(your_target pulicast::pulicast_asio)  # for single-threaded projects
target_link_libraries(your_target pulicast::pulicast_asio_multithreaded)  # for multi-threaded projects
```

## Pulicast aspects specific to C++

### Event-Loops
While pulicast for Python supports multiple event-loops, pulicast for C++ only supports 
[Asio](https://think-async.com/Asio/) as of now ([more information about event-loops and why they
are needed by pulicast can be found here](https://kiteswarms.gitlab.io/pulicast-python/concepts/threads_and_eventloops.html#pulicast-and-event-loops)).

Therefore the `pulicast::AsioNode` is the only node implementation in C++ right now.
It will not automatically start the event-loop for you so you will have to call 
`node.GetService()->run()` to ensure that incoming messages are processed and the node is announced 
in the network.


### Acquiring Channels

When acquiring a channel, we can only store a reference or pointer to it since it can not be copied:
```cpp
auto& channel = node["my_channel_name"];
auto channel_pointer = &channels["my_channel_name"];
```
This would fail to compile:
```cpp
auto channel = node["my_channel_name"]
```
It is advised to store references to channels and not pointers since pointer arithmetics on channels
make no sense.


### MessageInfo Struct

Instead of passing the `lead` and the `source` to the subscription handlers as direct parameters,
those are wrapped in a `pulicast::core::MessageInfo` struct.
This allows for easily adding new message information in the future without changing the 
subscription handlers signature.

### Message Serialization

Pulicast can send and receive all messages, for which a special encoding and decoding function has 
been defined.
Pulicast includes a templated encode-decode pair, that works for all ZCM types (`serialization.h`).
To add support for custom types, write template specializations of that encode-decode pair.
It is advised to just wrap your type in a ZCM type and then use the original encode-decode 
pair to do the actual encoding/decoding work.
As an example consider the template specializations in `serialization.h` for various primitive data
types.
If you want to add support for another group of types and that group of types shares a namespace,
then you can also write your own templated encode-decode pair in that namespace to avoid duplicate
function definitions.

#### Decoding

The decoding functions have the signature 
`void DecodeMessageInto(const asio::const_buffer &buffer, std::optional<T> &target)`.
Their task is to decode a message from a buffer and place it into a target `std::optional`.
If decoding fails, a `std::nullopt` should be returned.
The following example decodes a `std::string`:
```cpp
inline void DecodeMessageInto(const asio::const_buffer& buffer, std::optional<std::string> &target) {
  target = std::string(asio::buffer_cast<const char*>(buffer), buffer.size());
}
```
Note that here we just decode a string from the raw bytes in the buffer.
It would be better to wrap the `std::string` in a ZCM message since then it has a header with the
message type. This allows type inference for puliconf and generic subscriptions in languages with
dynamic typing (such as python).

You might wonder why not just write a `std::optional<T> Decode(const asio::const_buffer& buffer)`?
The issue is, that when overloading functions, the return type is not considered.
So overloading `Decode` with just varying return types would result in duplicate function 
definitions.
By introducing the output parameter, we ensure that they all have a different signature.

#### Encoding

The encoding functions have the signature
`template<class CallbackT> void EncodeAndThenCall(const T &message, CallbackT &&callback)`.
Their task is to encode a message to `pulicast::core::ScatterGatherBuffer` and call the callback
with that scatter-gather buffer.
For `std::string` it could look like this:
```cpp
template<class CallbackT>
inline void EncodeAndThenCall(const std::string &message, CallbackT&& callback) {
  callback(pulicast::core::ScatterGatherBuffer{{message.data(), message.size()}});
}
```
You might wonder what this convoluted way to encode messages is for.
The reason is, that we want to avoid any heap allocations to prevent memory fragmentation in an 
embedded setting.

Why not have a simple `BufferType Encode(const MessageType& msg) {...}` function and overload that
for each possible message type?
The problems lie in the design the above `BufferType`. 
It could be of fixed length, but then it needs to be large enough to contain any possible message, 
which is very wasteful.
Or it could be of dynamic length, but then it needs to be allocated on the heap, which we want to 
avoid.
The `pulicast::core::ScatterGatherBuffer` comes to the rescue here.
It is our solution for dynamic length buffers stored on the stack.
However, since its content is (potentially) stored on the stack, it must not be returned from a function.
This would lead to invalid pointers within the `ScatterGatherBuffer`!
Instead, we must pass it to a functor, which can then make further use of it 
for example by extending it and eventually passing it to some socket for sending.
It could be used like this:
```cpp
EncodeAndThenCall(std::string("my message"), [&](const auto& buf) {send_to_socket(socket, buf);});
```

`EncodeAndThenCall` implementations for ZCM messages, and a number of primitives types are contained
`serialization.h`.
They are include in the default `pulicast.h`.

To learn how to provide your own implementations of `EncodeAndThenCall`, look at the 
`custom_serialization.cpp` example.

### Multi-Threading
The `pulicast::AsioNode` is partially thread safe.
You can publish concurrently from any thread, however publishing might block when the operating
system queue of the udp socket is full.
Subscribing is **not** thread safe. It is advised that multi-threaded applications have a 
single-threaded initialization phase where all the subscriptions are set up.
Subscription handlers are called from inside the `asio::io_service::run()` method. 
When there are multiple threads execution the `run()` method, the subscription handlers can be 
called from any of those threads. 
To prevent a set of subscription handlers to be called concurrently, they can be wrapped in a common
[`asio::strand`](https://think-async.com/Asio/asio-1.18.0/doc/asio/reference/strand.html).
Unfortunately we can not directly wrap a subscription handler using `asio::strand::wrap()`. 
The helper function `pulicast::transport::wrap()` can be used instead.
Look at the `subscribe_with_strands` and the  `multi_threading_with_strands` examples to learn the
usage of the `pulicast::transport::wrap()` method and `asio::strand`.   

<!-- TODO(max): Custom transports -->
<!-- TODO(max): Custom rand function -->